/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Franggi Rodriguez
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({uy.ort.edu.Obl2.dominio.SistemaTest.class,
    uy.ort.edu.Obl2.dominio.ResultadoTest.class, uy.ort.edu.Obl2.dominio.PreguntaTest.class})
public class DominioSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

}
