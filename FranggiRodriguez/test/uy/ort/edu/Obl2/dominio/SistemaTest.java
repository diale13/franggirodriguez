/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import uy.ort.edu.Obl2.auxiliar.Parser;

/**
 *
 * @author Romina
 */
public class SistemaTest {

    public SistemaTest() {
    }

    Sistema sistema;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    private static ArrayList<Pregunta> cargarPreguntas() {
        ArrayList<Pregunta> preguntas = new ArrayList<>();

        Pregunta pregunta1 = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                listaVaciaString(),
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                listaVaciaString(),
                Pregunta.Tipo.CORTA,
                10);

        ArrayList<String> coloresPrimarios = coloresPrimarios();

        ArrayList<String> comentariosColoresPrimarios = new ArrayList<>();
        comentariosColoresPrimarios.add("¡Muy bien!");
        comentariosColoresPrimarios.add("¡Incorrecto! Esos son los colores secundarios.");
        comentariosColoresPrimarios.add("¡Incorrecto!");

        Pregunta pregunta2 = new Pregunta(
                "Pregunta 2",
                "¿Cuáles son los colores primarios?",
                coloresPrimarios,
                "Azul, rojo, amarillo",
                comentariosColoresPrimarios,
                Pregunta.Tipo.MULTIPLE,
                10);

        Pregunta pregunta3 = new Pregunta(
                "Pregunta 3",
                "El verde, ¿es un color primario?",
                listaVaciaString(),
                "F",
                listaVaciaString(),
                Pregunta.Tipo.VERDADERO_FALSO,
                10);

        Pregunta pregunta4 = new Pregunta(
                "Pregunta 4",
                "El azul, ¿es un color primario?",
                listaVaciaString(),
                "T",
                listaVaciaString(),
                Pregunta.Tipo.VERDADERO_FALSO,
                10);

        preguntas.add(pregunta1);
        preguntas.add(pregunta2);
        preguntas.add(pregunta3);
        preguntas.add(pregunta4);

        return preguntas;
    }

    private static ArrayList<String> coloresPrimarios() {
        ArrayList<String> coloresPrimarios = new ArrayList<>();
        coloresPrimarios.add("Azul, rojo, amarillo");
        coloresPrimarios.add("Violeta, naranja, verde");
        coloresPrimarios.add("Azul, verde, blanco");
        return coloresPrimarios;
    }

    private static ArrayList<Resultado> cargarRespuestas() {
        ArrayList<Resultado> respuestas = new ArrayList<>();

        final Resultado respuesta1_correcta = new Resultado(
                3000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "3",
                true);

        final Resultado respuesta1_incorrecta = new Resultado(
                6000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "Azul, rojo, amarillo",
                false);

        final Resultado respuesta1_sinTiempo = new Resultado(
                10000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "",
                false);

        final Resultado respuesta2_correcta = new Resultado(
                3000,
                "¿Cuáles son los colores primarios?",
                "Azul, rojo, amarillo",
                "Azul, rojo, amarillo",
                true);

        final Resultado respuesta2_incorrecta = new Resultado(
                5000,
                "¿Cuántos son los colores primarios?",
                "Azul, rojo, amarillo",
                "Violeta, naranja, verde",
                false);

        final Resultado respuesta2_sinTiempo = new Resultado(
                10000,
                "¿Cuántos son los colores primarios?",
                "Azul, rojo, amarillo",
                "",
                false);

        final Resultado respuesta3_correcta = new Resultado(
                3000,
                "El verde, ¿es un color primario?",
                "F",
                "F",
                true);

        final Resultado respuesta3_incorrecta = new Resultado(
                7000,
                "El verde, ¿es un color primario?",
                "F",
                "T",
                false);

        final Resultado respuesta3_sinTiempo = new Resultado(
                10000,
                "El verde, ¿es un color primario?",
                "F",
                "",
                false);

        final Resultado respuesta4_correcta = new Resultado(
                4000,
                "El azul, ¿es un color primario?",
                "T",
                "T",
                true);

        final Resultado respuesta4_incorrecta = new Resultado(
                2000,
                "El azul, ¿es un color primario?",
                "T",
                "F",
                false);

        final Resultado respuesta4_sinTiempo = new Resultado(
                10000,
                "El azul, ¿es un color primario?",
                "T",
                "",
                false);

        respuestas.add(respuesta1_correcta);
        respuestas.add(respuesta1_incorrecta);
        respuestas.add(respuesta1_sinTiempo);
        respuestas.add(respuesta2_correcta);
        respuestas.add(respuesta2_incorrecta);
        respuestas.add(respuesta2_sinTiempo);
        respuestas.add(respuesta3_correcta);
        respuestas.add(respuesta3_incorrecta);
        respuestas.add(respuesta3_sinTiempo);
        respuestas.add(respuesta4_correcta);
        respuestas.add(respuesta4_incorrecta);
        respuestas.add(respuesta4_sinTiempo);

        return respuestas;
    }

    private static ArrayList<String> listaVaciaString() {
        return new ArrayList<>();
    }

    private static ArrayList<Pregunta> listaVaciaPregunta() {
        return new ArrayList<>();
    }

    /**
     * Test of getTiempoPromedio method, of class Sistema.
     */
    @Test
    public void testGetTiempoPromedioConRespuestasCorrectas() {
        sistema = new Sistema();
        sistema.setCantidadPreguntas(4);
        sistema.setTiempoPreguntas(13000);
        System.out.println("getTiempoPromedio");
        int expResult = 3;
        int result = sistema.getTiempoPromedio();
        assertEquals(expResult, result);
    }

    /**
     * Test of obtenerSistemaGlobal method, of class Sistema.
     */
    @Test
    public void testObtenerSistemaGlobal() {
        System.out.println("obtenerSistemaGlobal");
        sistema = new Sistema();
        Sistema.setSistemaUnico(sistema);
        Sistema expResult = sistema;
        Sistema result = Sistema.obtenerSistemaGlobal();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCantidadRespuestasCorrectas method, of class Sistema.
     */
    @Test
    public void testGetCantidadRespuestasCorrectas() {
        System.out.println("getCantidadRespuestasCorrectas");
        Sistema instance = new Sistema();
        instance.setCantidadRespuestasCorrectas(0);
        int expResult = 0;
        int result = instance.getCantidadRespuestasCorrectas();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCantidadPreguntas method, of class Sistema.
     */
    @Test
    public void testGetCantidadPreguntas() {
        System.out.println("getCantidadPreguntas");
        Sistema instance = new Sistema();
        instance.setCantidadPreguntas(0);
        int expResult = 0;
        int result = instance.getCantidadPreguntas();
        assertEquals(expResult, result);
    }

    /**
     * Test of hayMasPreguntas method, of class Sistema.
     */
    @Test
    public void testHayMasPreguntas_casoTrue() {
        System.out.println("hayMasPreguntas");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> preguntas = cargarPreguntas();
        instance.setListaPreguntas(preguntas);
        boolean expResult = true;
        boolean result = instance.hayMasPreguntas();
        assertEquals(expResult, result);
    }

    /**
     * Test of hayMasPreguntas method, of class Sistema.
     */
    @Test
    public void testHayMasPreguntas_casoFalse() {
        System.out.println("hayMasPreguntas");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> preguntas = new ArrayList<>();
        instance.setListaPreguntas(preguntas);
        boolean expResult = false;
        boolean result = instance.hayMasPreguntas();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaPreguntas method, of class Sistema.
     */
    @Test
    public void testGetListaPreguntas() {
        System.out.println("getListaPreguntas");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> preguntas = cargarPreguntas();
        instance.setListaPreguntas(preguntas);
        ArrayList<Pregunta> expResult = preguntas;
        ArrayList<Pregunta> result = instance.getListaPreguntas();
        assertEquals(expResult, result);
    }

    /**
     * Test of limpiarSistema method, of class Sistema.
     */
    @Test
    public void testLimpiarSistema() {
        System.out.println("limpiarSistema");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> preguntas = cargarPreguntas();
        instance.setListaPreguntas(preguntas);
        instance.setManejadorBarraProgreso(preguntas);
        instance.limpiarSistema();
    }

    /**
     * Test of getManejadorBarraProgreso method, of class Sistema.
     */
    @Test
    public void testGetManejadorBarraProgreso() {
        System.out.println("getManejadorBarraProgreso");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> nueva = listaVaciaPregunta();
        instance.setManejadorBarraProgreso(nueva);
        ArrayList<Pregunta> expResult = nueva;
        ArrayList<Pregunta> result = instance.getManejadorBarraProgreso();
        assertEquals(expResult, result);
    }

    /**
     * Test of siguientePregunta method, of class Sistema.
     */
    @Test
    public void testSiguientePregunta() {
        System.out.println("siguientePregunta");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> listaPreguntas = cargarPreguntas();
        instance.setListaPreguntas(listaPreguntas);
        Pregunta expResult = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                listaVaciaString(),
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                listaVaciaString(),
                Pregunta.Tipo.CORTA,
                10);
        Pregunta result = instance.siguientePregunta();
        assertEquals(expResult, result);
    }

    /**
     * Test of eliminarPrimerPreguntaCargada method, of class Sistema.
     */
    @Test
    public void testEliminarPrimerPreguntaCargada() {
        System.out.println("eliminarPrimerPreguntaCargada");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> listaPreguntas = cargarPreguntas();
        instance.setListaPreguntas(listaPreguntas);
        instance.eliminarPrimerPreguntaCargada();
    }

    @Test
    public void testEliminarPrimerPreguntaCargada_isEmpty() {
        System.out.println("eliminarPrimerPreguntaCargada");
        Sistema instance = new Sistema();
        instance.setListaPreguntas(new ArrayList<>());
        instance.eliminarPrimerPreguntaCargada();
    }

    /**
     * Test of sumarContadorRespuestaCorrecta method, of class Sistema.
     */
    @Test
    public void testSumarContadorRespuestaCorrecta() {
        System.out.println("sumarContadorRespuestaCorrecta");
        Sistema instance = new Sistema();
        instance.setCantidadRespuestasCorrectas(0);
        instance.sumarContadorRespuestaCorrecta();
    }

    /**
     * Test of agregarResultado method, of class Sistema.
     */
    @Test
    public void testAgregarResultado() {
        System.out.println("agregarResultado");
        Resultado resultado = new Resultado(0, "", "", "", true);
        Sistema instance = new Sistema();
        ArrayList<Resultado> respuestas = cargarRespuestas();
        instance.setRegistro(respuestas);
        instance.agregarResultado(resultado);
    }

    @Test
    public void testAgregarResultado_null() {
        System.out.println("agregarResultado");
        Resultado resultado = new Resultado(0, "", "", "", true);
        Sistema instance = new Sistema();
        instance.setRegistro(null);
        instance.agregarResultado(resultado);
    }

    /**
     * Test of getRegistro method, of class Sistema.
     */
    @Test
    public void testGetRegistro() {
        System.out.println("getRegistro");
        Sistema instance = new Sistema();
        ArrayList<Resultado> respuestas = cargarRespuestas();
        instance.setRegistro(respuestas);
        ArrayList<Resultado> expResult = respuestas;
        ArrayList<Resultado> result = instance.getRegistro();
        assertEquals(expResult, result);
    }

    /**
     * Test of getParserGlobal method, of class Sistema.
     */
    @Test
    public void testGetParserGlobal() {
        System.out.println("getParserGlobal");
        Sistema instance = new Sistema();
        Parser expResult = new Parser();
        instance.setParserGlobal(expResult);
        Parser result = instance.getParserGlobal();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetParserGlobal_null() {
        System.out.println("getParserGlobal");
        Sistema instance = new Sistema();
        instance.setParserGlobal(null);
        Parser result = instance.getParserGlobal();
        Parser expResult = instance.getParserGlobal();
        assertEquals(expResult, result);
    }

    /**
     * Test of setParserGlobal method, of class Sistema.
     */
    @Test
    public void testSetParserGlobal() {
        System.out.println("setParserGlobal");
        Parser parser = new Parser();
        Sistema instance = new Sistema();
        instance.setParserGlobal(parser);
    }

    /**
     * Test of agregarPreguntaAListaPreguntas method, of class Sistema.
     */
    @Test
    public void testAgregarPreguntaAListaPreguntas() {
        System.out.println("agregarPreguntaAListaPreguntas");
        Pregunta pregunta = new Pregunta("", "", listaVaciaString(),
                "", listaVaciaString(), Pregunta.Tipo.MULTIPLE, 0);
        ArrayList<Pregunta> listaPreguntas = cargarPreguntas();
        Sistema instance = new Sistema();
        instance.setListaPreguntas(listaPreguntas);
        instance.agregarPreguntaAListaPreguntas(pregunta);
    }

    /**
     * Test of agregarPreguntaAListaManejadorBarra method, of class Sistema.
     */
    @Test
    public void testAgregarPreguntaAListaManejadorBarra() {
        System.out.println("agregarPreguntaAListaManejadorBarra");
        Pregunta pregunta = new Pregunta("", "", listaVaciaString(),
                "", listaVaciaString(), Pregunta.Tipo.MULTIPLE, 0);
        ArrayList<Pregunta> listaPreguntas = cargarPreguntas();
        Sistema instance = new Sistema();
        instance.setManejadorBarraProgreso(listaPreguntas);
        instance.agregarPreguntaAListaManejadorBarra(pregunta);
    }

    /**
     * Test of setCantidadRespuestasCorrectas method, of class Sistema.
     */
    @Test
    public void testSetCantidadRespuestasCorrectas() {
        System.out.println("setCantidadRespuestasCorrectas");
        int cantidadRespuestasCorrectas = 0;
        Sistema instance = new Sistema();
        instance.setCantidadRespuestasCorrectas(cantidadRespuestasCorrectas);
    }

    /**
     * Test of setCantidadPreguntas method, of class Sistema.
     */
    @Test
    public void testSetCantidadPreguntas() {
        System.out.println("setCantidadPreguntas");
        int cantidadPreguntas = 0;
        Sistema instance = new Sistema();
        instance.setCantidadPreguntas(cantidadPreguntas);
    }

    /**
     * Test of setManejadorBarraProgreso method, of class Sistema.
     */
    @Test
    public void testSetManejadorBarraProgreso() {
        System.out.println("setManejadorBarraProgreso");
        ArrayList<Pregunta> manejadorBarraProgreso = null;
        Sistema instance = new Sistema();
        instance.setManejadorBarraProgreso(manejadorBarraProgreso);
    }

    /**
     * Test of setTiempoPreguntas method, of class Sistema.
     */
    @Test
    public void testSetTiempoPreguntas() {
        System.out.println("setTiempoPreguntas");
        int tiempoPreguntas = 0;
        Sistema instance = new Sistema();
        instance.setTiempoPreguntas(tiempoPreguntas);
    }

    /**
     * Test of getTiempoPromedio method, of class Sistema.
     */
    @Test
    public void testGetTiempoPromedio() {
        System.out.println("getTiempoPromedio");
        Sistema instance = new Sistema();
        instance.setTiempoPreguntas(1000000);
        instance.setCantidadPreguntas(100);
        int expResult = 10;
        int result = instance.getTiempoPromedio();
        assertEquals(expResult, result);
    }

    /**
     * Test of hayMasPreguntas method, of class Sistema.
     */
    @Test
    public void testHayMasPreguntas_true() {
        System.out.println("hayMasPreguntas");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> listaPreguntas = cargarPreguntas();
        instance.setListaPreguntas(listaPreguntas);
        boolean expResult = true;
        boolean result = instance.hayMasPreguntas();
        assertEquals(expResult, result);
    }

    @Test
    public void testHayMasPreguntas_false() {
        System.out.println("hayMasPreguntas");
        Sistema instance = new Sistema();
        instance.setListaPreguntas(listaVaciaPregunta());
        boolean expResult = false;
        boolean result = instance.hayMasPreguntas();
        assertEquals(expResult, result);
    }

    /**
     * Test of crearListaPreguntasVacia method, of class Sistema.
     */
    @Test
    public void testCrearListaPreguntasVacia_null() {
        System.out.println("crearListaPreguntasVacia");
        Sistema instance = new Sistema();
        instance.setListaPreguntas(null);
        instance.crearListaPreguntasVacia();
    }

    @Test
    public void testCrearListaPreguntasVacia_noNull() {
        System.out.println("crearListaPreguntasVacia");
        Sistema instance = new Sistema();
        instance.setListaPreguntas(cargarPreguntas());
        instance.crearListaPreguntasVacia();
    }

    /**
     * Test of crearManejadorBarraProgresoVacio method, of class Sistema.
     */
    @Test
    public void testCrearManejadorBarraProgresoVacio() {
        System.out.println("crearManejadorBarraProgresoVacio");
        Sistema instance = new Sistema();
        instance.crearManejadorBarraProgresoVacio();
    }

    /**
     * Test of crearRegistroVacio method, of class Sistema.
     */
    @Test
    public void testCrearRegistroVacio_null() {
        System.out.println("crearRegistroVacio");
        Sistema instance = new Sistema();
        instance.setRegistro(null);
        instance.crearRegistroVacio();
    }

    @Test
    public void testCrearRegistroVacio_noNull() {
        System.out.println("crearRegistroVacio");
        Sistema instance = new Sistema();
        instance.setRegistro(cargarRespuestas());
        instance.crearRegistroVacio();
    }

    /**
     * Test of sumarContadorPregunta method, of class Sistema.
     */
    @Test
    public void testSumarContadorPregunta() {
        System.out.println("sumarContadorPregunta");
        Sistema instance = new Sistema();
        instance.setCantidadPreguntas(0);
        instance.sumarContadorPregunta();
    }

    /**
     * Test of setListaPreguntas method, of class Sistema.
     */
    @Test
    public void testSetListaPreguntas() {
        System.out.println("setListaPreguntas");
        ArrayList<Pregunta> listaPreguntas = cargarPreguntas();
        Sistema instance = new Sistema();
        instance.setListaPreguntas(listaPreguntas);
    }

    /**
     * Test of setRegistro method, of class Sistema.
     */
    @Test
    public void testSetRegistro() {
        System.out.println("setRegistro");
        ArrayList<Resultado> registro = cargarRespuestas();
        Sistema instance = new Sistema();
        instance.setRegistro(registro);
    }

    /**
     * Test of setSistemaUnico method, of class Sistema.
     */
    @Test
    public void testSetSistemaUnico() {
        System.out.println("setSistemaUnico");
        Sistema sistemaUnico = null;
        Sistema.setSistemaUnico(sistemaUnico);
    }

}
