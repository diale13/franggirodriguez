/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Franggi Rodriguez
 */
public class PreguntaTest {

    public PreguntaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Prueba de metodo: getPregunta, of de clase Pregunta.
     */
    @Test
    public void testGetPregunta() {
        System.out.println("getPregunta");
        Pregunta instance = new Pregunta("", "Pregunta", null, "", null, Pregunta.Tipo.MULTIPLE, 0);
        String expResult = "Pregunta";
        String result = instance.getPregunta();
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba de metodo: valorVoF, de clase Pregunta.
     */
    @Test
    public void testValues_VoF() {
        System.out.println("Values_VoF");
        Pregunta.Tipo tipoVoF = Pregunta.Tipo.valueOf("VERDADERO_FALSO");
        assertEquals(tipoVoF, Pregunta.Tipo.VERDADERO_FALSO);
    }

    /**
     * *
     * Prueba de metodo: valor multiple, de clase Pregunta.
     */
    @Test
    public void testValues_Multiple() {
        System.out.println("Values_Multiple");
        Pregunta.Tipo tipoMultiple = Pregunta.Tipo.valueOf("MULTIPLE");
        assertEquals(tipoMultiple, Pregunta.Tipo.MULTIPLE);
    }

    /**
     * *
     * Prueba de metodo: valor corta, de clase Pregunta.
     */
    @Test
    public void testValues_Corta() {
        System.out.println("Values_Corta");
        Pregunta.Tipo tipoCorta = Pregunta.Tipo.valueOf("CORTA");
        assertEquals(tipoCorta, Pregunta.Tipo.CORTA);
    }

    /**
     * *
     * Carga una seleccion de preguntas para las pruebas
     *
     * @return retorna arraylist de preguntas cargadas
     */
    private static ArrayList<Pregunta> cargarPreguntas() {
        ArrayList<String> listaVacia = new ArrayList<>();
        ArrayList<String> coloresPrimarios = coloresPrimarios();

        ArrayList<String> comentariosColoresPrimarios = new ArrayList<>();
        comentariosColoresPrimarios.add("¡Muy bien!");
        comentariosColoresPrimarios.add("¡Incorrecto! Esos son los colores secundarios.");
        comentariosColoresPrimarios.add("¡Incorrecto!");
        Pregunta pregunta1 = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                listaVacia,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                listaVacia,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta pregunta2 = new Pregunta(
                "Pregunta 2",
                "¿Cuáles son los colores primarios?",
                coloresPrimarios,
                "Azul, rojo, amarillo",
                comentariosColoresPrimarios,
                Pregunta.Tipo.MULTIPLE,
                10);
        Pregunta pregunta3 = new Pregunta(
                "Pregunta 3",
                "El verde, ¿es un color primario?",
                listaVacia,
                "F",
                listaVacia,
                Pregunta.Tipo.VERDADERO_FALSO,
                10);
        Pregunta pregunta4 = new Pregunta(
                "Pregunta 4",
                "El azul, ¿es un color primario?",
                listaVacia,
                "T",
                listaVacia,
                Pregunta.Tipo.VERDADERO_FALSO,
                10);
        ArrayList<Pregunta> preguntas = new ArrayList<>();
        preguntas.add(pregunta1);
        preguntas.add(pregunta2);
        preguntas.add(pregunta3);
        preguntas.add(pregunta4);
        return preguntas;
    }

    /**
     * *
     * Carga una seleccion de colores primarios para las pruebas
     *
     * @return retorna arraylist de colores cargadas
     */
    private static ArrayList<String> coloresPrimarios() {
        ArrayList<String> coloresPrimarios = new ArrayList<>();
        coloresPrimarios.add("Azul, rojo, amarillo");
        coloresPrimarios.add("Violeta, naranja, verde");
        coloresPrimarios.add("Azul, verde, blanco");
        return coloresPrimarios;
    }

    /**
     * *
     * Carga una seleccion de respuestas para las pruebas
     *
     * @return retorna arraylist de respuestas cargadas
     */
    private static ArrayList<Resultado> cargarRespuestas() {
        ArrayList<Resultado> respuestas = new ArrayList<>();
        final Resultado respuesta1_correcta = new Resultado(
                3000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "3",
                true);

        final Resultado respuesta1_incorrecta = new Resultado(
                6000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "Azul, rojo, amarillo",
                false);

        final Resultado respuesta1_sinTiempo = new Resultado(
                10000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "",
                false);

        final Resultado respuesta2_correcta = new Resultado(
                3000,
                "¿Cuáles son los colores primarios?",
                "Azul, rojo, amarillo",
                "Azul, rojo, amarillo",
                true);

        final Resultado respuesta2_incorrecta = new Resultado(
                5000,
                "¿Cuántos son los colores primarios?",
                "Azul, rojo, amarillo",
                "Violeta, naranja, verde",
                false);

        final Resultado respuesta2_sinTiempo = new Resultado(
                10000,
                "¿Cuántos son los colores primarios?",
                "Azul, rojo, amarillo",
                "",
                false);

        final Resultado respuesta3_correcta = new Resultado(
                3000,
                "El verde, ¿es un color primario?",
                "F",
                "F",
                true);

        final Resultado respuesta3_incorrecta = new Resultado(
                7000,
                "El verde, ¿es un color primario?",
                "F",
                "T",
                false);

        final Resultado respuesta3_sinTiempo = new Resultado(
                10000,
                "El verde, ¿es un color primario?",
                "F",
                "",
                false);

        final Resultado respuesta4_correcta = new Resultado(
                4000,
                "El azul, ¿es un color primario?",
                "T",
                "T",
                true);

        final Resultado respuesta4_incorrecta = new Resultado(
                2000,
                "El azul, ¿es un color primario?",
                "T",
                "F",
                false);

        final Resultado respuesta4_sinTiempo = new Resultado(
                10000,
                "El azul, ¿es un color primario?",
                "T",
                "",
                false);

        respuestas.add(respuesta1_correcta);
        respuestas.add(respuesta1_incorrecta);
        respuestas.add(respuesta1_sinTiempo);
        respuestas.add(respuesta2_correcta);
        respuestas.add(respuesta2_incorrecta);
        respuestas.add(respuesta2_sinTiempo);
        respuestas.add(respuesta3_correcta);
        respuestas.add(respuesta3_incorrecta);
        respuestas.add(respuesta3_sinTiempo);
        respuestas.add(respuesta4_correcta);
        respuestas.add(respuesta4_incorrecta);
        respuestas.add(respuesta4_sinTiempo);

        return respuestas;
    }

    /**
     * Prueba de getTitulo, de clase Pregunta.
     */
    @Test
    public void testGetTitulo() {
        System.out.println("getTitulo");
        Pregunta instance = new Pregunta("Titulo", "", null, "", null,
                Pregunta.Tipo.MULTIPLE, 0);
        String expResult = "Titulo";
        String result = instance.getTitulo();
        assertEquals(expResult, result);
    }

    /**
     * Prueba de getRespuestas, de clase Pregunta.
     */
    @Test
    public void testGetRespuestas() {
        System.out.println("getRespuestas");
        ArrayList<String> coloresPrimarios = coloresPrimarios();

        Pregunta instance = new Pregunta(
                "Pregunta 2",
                "¿Cuáles son los colores primarios?",
                coloresPrimarios,
                "Azul, rojo, amarillo",
                null,
                Pregunta.Tipo.MULTIPLE,
                10);
        ArrayList<String> expResult = coloresPrimarios();
        ArrayList<String> result = instance.getRespuestas();
        assertEquals(expResult, result);
    }

    /**
     * Prueba de getRespuestaCorrecta, de clase Pregunta.
     */
    @Test
    public void testGetRespuestaCorrecta() {
        System.out.println("getRespuestaCorrecta");
        Pregunta instance = new Pregunta("", "", null, "RespuestaCorrecta",
                null, Pregunta.Tipo.MULTIPLE, 0);
        String expResult = "RespuestaCorrecta";
        String result = instance.getRespuestaCorrecta();
        assertEquals(expResult, result);
    }

    /**
     * Prueba de getComentario, de clase Pregunta.
     */
    @Test
    public void testGetComentario() {
        System.out.println("getComentario");
        ArrayList<String> comentariosColoresPrimarios = new ArrayList<>();
        comentariosColoresPrimarios.add("¡Muy bien!");
        comentariosColoresPrimarios.add("¡Incorrecto! Esos son los colores secundarios.");
        comentariosColoresPrimarios.add("¡Incorrecto!");

        Pregunta instance = new Pregunta(
                "Pregunta 2",
                "¿Cuáles son los colores primarios?",
                null,
                "Azul, rojo, amarillo",
                comentariosColoresPrimarios,
                Pregunta.Tipo.MULTIPLE,
                10);
        ArrayList<String> expResult = comentariosColoresPrimarios;
        ArrayList<String> result = instance.getComentario();
        assertEquals(expResult, result);
    }

    /**
     * Prueba de metodo: getTipo() de clase Pregunta
     */
    @Test
    public void testGetTipo() {
        System.out.println("getTipo");
        Pregunta instance = new Pregunta("", "", null, "", null, Pregunta.Tipo.MULTIPLE, 0);
        Pregunta.Tipo expResult = Pregunta.Tipo.MULTIPLE;
        Pregunta.Tipo result = instance.getTipo();
        assertEquals(expResult, result);
    }

    /**
     * Prueba de metodo: equals de Pregunta
     */
    @Test
    public void testEquals() {
        System.out.println("equals");

        Pregunta instance = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba de metodo: objeto es nulo
     */
    @Test
    public void testEquals_ObjIsNull() {
        System.out.println("equals objIsNull");
        Pregunta obj = null;
        Pregunta instance = new Pregunta(
                "Pregunta 2",
                "¿Cuáles son los colores primarios?",
                null,
                "Azul, rojo, amarillo",
                null,
                Pregunta.Tipo.MULTIPLE,
                10);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba de metodo: equals distinta clase
     */
    @Test
    public void testEquals_ObjInstanceDifferentClass() {
        System.out.println("equals different class");
        String obj = "Hola";
        Pregunta instance = new Pregunta(
                "Pregunta 2",
                "¿Cuáles son los colores primarios?",
                null,
                "Azul, rojo, amarillo",
                null,
                Pregunta.Tipo.MULTIPLE,
                10);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba equals de distinto titulo
     */
    @Test
    public void testEquals_DifferentTitulo() {
        System.out.println("equals Different Tiempo");
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta instance = new Pregunta(
                "Pregunta 2",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba equals de distinta pregunta
     */
    @Test
    public void testEquals_DifferentPregunta() {
        System.out.println("equals Different Pregunta");
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta instance = new Pregunta(
                "Pregunta 1",
                "¿Cuáles son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba Equals de distinta respuesta correcta
     */
    @Test
    public void testEquals_DifferentRespuestaCorrecta() {
        System.out.println("equals Different RespuestaCorrecta");
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta instance = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Azul, rojo, amarillo",
                null,
                Pregunta.Tipo.CORTA,
                10);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba Equals de distinto tiempo
     */
    @Test
    public void testEquals_DifferentTiempo() {
        System.out.println("equals Different Tiempo");
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta instance = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                15);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba equals de distinto tipo
     */
    @Test
    public void testEquals_DifferentTipo() {
        System.out.println("equals Different Tiempo");
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta instance = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.MULTIPLE,
                10);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba equals de distinta respuesta
     *
     */
    @Test
    public void testEquals_DifferentRespuesta() {
        System.out.println("equals Different Tiempo");
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta instance = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                coloresPrimarios(),
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * *
     * Prueba Equals de distinto comentario
     */
    @Test
    public void testEquals_DifferentComentario() {
        System.out.println("equals Different Tiempo");
        Object obj = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                null,
                Pregunta.Tipo.CORTA,
                10);
        Pregunta instance = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                null,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                coloresPrimarios(),
                Pregunta.Tipo.CORTA,
                10);

        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Prueba de getTiempo, de clase Pregunta.
     */
    @Test
    public void testGetTiempo() {
        System.out.println("getTiempo");
        Pregunta instance = new Pregunta("Titulo", "", null, "", null,
                Pregunta.Tipo.MULTIPLE, 0);
        int expResult = 0;
        int result = instance.getTiempo();
        assertEquals(expResult, result);
    }

}
