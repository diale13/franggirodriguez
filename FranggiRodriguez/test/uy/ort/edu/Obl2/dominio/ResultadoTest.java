/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Franggi Rodriguez
 */
public class ResultadoTest {

    public ResultadoTest() {
    }

    Sistema sistema;

    @BeforeClass
    public static void setUpClass() {
        ArrayList<Pregunta> preguntas = cargarPreguntas();
        ArrayList<Resultado> respuestas = cargarRespuestas();

        Sistema sistema = Sistema.obtenerSistemaGlobal();
        sistema.setListaPreguntas(preguntas);
        sistema.setRegistro(respuestas);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * *
     * Carga una seleccion de preguntas para las prebas
     *
     * @return un arraylist de preguntas
     */
    private static ArrayList<Pregunta> cargarPreguntas() {
        ArrayList<String> listaVacia = new ArrayList<>();
        ArrayList<String> coloresPrimarios = coloresPrimarios();
        ArrayList<String> comentariosColoresPrimarios = new ArrayList<>();
        comentariosColoresPrimarios.add("¡Muy bien!");
        comentariosColoresPrimarios.add("¡Incorrecto! Esos son los colores secundarios.");
        comentariosColoresPrimarios.add("¡Incorrecto!");

        Pregunta pregunta1 = new Pregunta(
                "Pregunta 1",
                "¿Cuántos son los colores primarios?",
                listaVacia,
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                listaVacia,
                Pregunta.Tipo.CORTA,
                10);

        Pregunta pregunta2 = new Pregunta(
                "Pregunta 2",
                "¿Cuáles son los colores primarios?",
                coloresPrimarios,
                "Azul, rojo, amarillo",
                comentariosColoresPrimarios,
                Pregunta.Tipo.MULTIPLE,
                10);

        Pregunta pregunta3 = new Pregunta(
                "Pregunta 3",
                "El verde, ¿es un color primario?",
                listaVacia,
                "F",
                listaVacia,
                Pregunta.Tipo.VERDADERO_FALSO,
                10);

        Pregunta pregunta4 = new Pregunta(
                "Pregunta 4",
                "El azul, ¿es un color primario?",
                listaVacia,
                "T",
                listaVacia,
                Pregunta.Tipo.VERDADERO_FALSO,
                10);
        ArrayList<Pregunta> preguntas = new ArrayList<>();
        preguntas.add(pregunta1);
        preguntas.add(pregunta2);
        preguntas.add(pregunta3);
        preguntas.add(pregunta4);

        return preguntas;
    }

    /**
     * *
     * Carga una seleccion de colores primarios para las pruebas
     *
     * @return retorna arraylist de colores cargadas
     */
    private static ArrayList<String> coloresPrimarios() {
        ArrayList<String> coloresPrimarios = new ArrayList<>();
        coloresPrimarios.add("Azul, rojo, amarillo");
        coloresPrimarios.add("Violeta, naranja, verde");
        coloresPrimarios.add("Azul, verde, blanco");
        return coloresPrimarios;
    }

    /**
     * *
     * Carga una seleccion de respuestas para las pruebas
     *
     * @return retorna arraylist de respuestas cargadas
     */
    private static ArrayList<Resultado> cargarRespuestas() {
        ArrayList<Resultado> respuestas = new ArrayList<>();

        final Resultado respuesta1_correcta = new Resultado(
                3000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "3",
                true);

        final Resultado respuesta1_incorrecta = new Resultado(
                6000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "Azul, rojo, amarillo",
                false);

        final Resultado respuesta1_sinTiempo = new Resultado(
                10000,
                "¿Cuántos son los colores primarios?",
                "Tres@splitargument@tres@splitargument@3@splitargument@TRES",
                "",
                false);

        final Resultado respuesta2_correcta = new Resultado(
                3000,
                "¿Cuáles son los colores primarios?",
                "Azul, rojo, amarillo",
                "Azul, rojo, amarillo",
                true);

        final Resultado respuesta2_incorrecta = new Resultado(
                5000,
                "¿Cuántos son los colores primarios?",
                "Azul, rojo, amarillo",
                "Violeta, naranja, verde",
                false);

        final Resultado respuesta2_sinTiempo = new Resultado(
                10000,
                "¿Cuántos son los colores primarios?",
                "Azul, rojo, amarillo",
                "",
                false);

        final Resultado respuesta3_correcta = new Resultado(
                3000,
                "El verde, ¿es un color primario?",
                "F",
                "F",
                true);

        final Resultado respuesta3_incorrecta = new Resultado(
                7000,
                "El verde, ¿es un color primario?",
                "F",
                "T",
                false);

        final Resultado respuesta3_sinTiempo = new Resultado(
                10000,
                "El verde, ¿es un color primario?",
                "F",
                "",
                false);

        final Resultado respuesta4_correcta = new Resultado(
                4000,
                "El azul, ¿es un color primario?",
                "T",
                "T",
                true);

        final Resultado respuesta4_incorrecta = new Resultado(
                2000,
                "El azul, ¿es un color primario?",
                "T",
                "F",
                false);

        final Resultado respuesta4_sinTiempo = new Resultado(
                10000,
                "El azul, ¿es un color primario?",
                "T",
                "",
                false);

        respuestas.add(respuesta1_correcta);
        respuestas.add(respuesta1_incorrecta);
        respuestas.add(respuesta1_sinTiempo);
        respuestas.add(respuesta2_correcta);
        respuestas.add(respuesta2_incorrecta);
        respuestas.add(respuesta2_sinTiempo);
        respuestas.add(respuesta3_correcta);
        respuestas.add(respuesta3_incorrecta);
        respuestas.add(respuesta3_sinTiempo);
        respuestas.add(respuesta4_correcta);
        respuestas.add(respuesta4_incorrecta);
        respuestas.add(respuesta4_sinTiempo);

        return respuestas;
    }

    /**
     * Test of isEsCorrecto method, of class Resultado.
     */
    @Test
    public void testIsEsCorrecto_True() {
        System.out.println("isEsCorrecto_true");
        Resultado instance = new Resultado(0, "", "", "", true);
        boolean expResult = true;
        boolean result = instance.isEsCorrecto();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEsCorrecto method, of class Resultado.
     */
    @Test
    public void testIsEsCorrecto_False() {
        System.out.println("isEsCorrecto_false");
        Resultado instance = new Resultado(0, "", "", "", false);
        boolean expResult = false;
        boolean result = instance.isEsCorrecto();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTiempo method, of class Resultado.
     */
    @Test
    public void testGetTiempo() {
        System.out.println("getTiempo");
        Resultado instance = new Resultado(0, "", "", "", false);
        int expResult = 0;
        int result = instance.getTiempo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPregunta method, of class Resultado.
     */
    @Test
    public void testGetPregunta() {
        System.out.println("getPregunta");
        Resultado instance = new Resultado(0, "Hola", "", "", false);
        String expResult = "Hola";
        String result = instance.getPregunta();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRespuestaCorrecta method, of class Resultado.
     */
    @Test
    public void testGetRespuestaCorrecta() {
        System.out.println("getRespuestaCorrecta");
        Resultado instance = new Resultado(0, "", "Hola", "", false);
        String expResult = "Hola";
        String result = instance.getRespuestaCorrecta();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRespuestaElegida method, of class Resultado.
     */
    @Test
    public void testGetRespuestaElegida() {
        System.out.println("getRespuestaElegida");
        Resultado instance = new Resultado(0, "", "", "Hola", false);
        String expResult = "Hola";
        String result = instance.getRespuestaElegida();
        assertEquals(expResult, result);
    }

}
