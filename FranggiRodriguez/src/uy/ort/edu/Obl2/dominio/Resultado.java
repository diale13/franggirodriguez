/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.dominio;

/**
 *
 * @author Franggi y Rodriguez
 */
public class Resultado {

    int tiempo;
    String pregunta;
    String respuestaCorrecta;
    String respuestaElegida;
    boolean esCorrecto;

    /**
     * *
     * Crea un objeto Resultado basado en los parametros
     *
     * @param tiempo el tiempo que se demoró en realizar la pregunta
     * @param pregunta la pregunta contestada
     * @param respuestaCorrecta la respuesta correcta de la pregunta realizada
     * @param respuestaElegida la respuesta elegida de la pregunta realizada
     * @param esCorrecto true si la pregunta fue correctamente respondida
     */
    public Resultado(int tiempo, String pregunta, String respuestaCorrecta,
            String respuestaElegida, boolean esCorrecto) {
        this.tiempo = tiempo;
        this.pregunta = pregunta;
        this.respuestaCorrecta = respuestaCorrecta;
        this.respuestaElegida = respuestaElegida;
        this.esCorrecto = esCorrecto;
    }

    public boolean isEsCorrecto() {
        return esCorrecto;
    }

    public int getTiempo() {
        return tiempo;
    }

    public String getPregunta() {
        return pregunta;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public String getRespuestaElegida() {
        return respuestaElegida;
    }
}
