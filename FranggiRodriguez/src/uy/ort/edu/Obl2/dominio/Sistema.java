package uy.ort.edu.Obl2.dominio;

import java.util.ArrayList;
import uy.ort.edu.Obl2.auxiliar.Parser;

/**
 * *
 *
 * @author Franggi y Rodriguez
 */
public class Sistema {

    private static Sistema sistemaUnico;
    private ArrayList<Pregunta> listaPreguntas;
    private ArrayList<Resultado> registro;
    private int cantidadRespuestasCorrectas;
    private int cantidadPreguntas;
    private ArrayList<Pregunta> manejadorBarraProgreso;
    private int tiempoPreguntas;
    private Parser parserGlobal;

    public Parser getParserGlobal() {
        if (parserGlobal == null) {
            this.parserGlobal = new Parser();
        }
        return parserGlobal;
    }

    public void setParserGlobal(Parser parser) {
        this.parserGlobal = parser;
    }

    public void agregarPreguntaAListaPreguntas(Pregunta pregunta) {
        this.listaPreguntas.add(pregunta);
    }

    public void agregarPreguntaAListaManejadorBarra(Pregunta pregunta) {
        this.manejadorBarraProgreso.add(pregunta);
    }

    public void setCantidadRespuestasCorrectas(int cantidadRespuestasCorrectas) {
        this.cantidadRespuestasCorrectas = cantidadRespuestasCorrectas;
    }

    public void setCantidadPreguntas(int cantidadPreguntas) {
        this.cantidadPreguntas = cantidadPreguntas;
    }

    public void setManejadorBarraProgreso(ArrayList<Pregunta> manejadorBarraProgreso) {
        this.manejadorBarraProgreso = manejadorBarraProgreso;
    }

    public void setTiempoPreguntas(int tiempoPreguntas) {
        this.tiempoPreguntas = tiempoPreguntas;
    }

    /**
     * *
     * Se crea un único sistema global en caso de no haber sido creado; caso
     * contrario se devuelve el existente.
     *
     * @return clase sistema global
     */
    public static Sistema obtenerSistemaGlobal() {
        if (sistemaUnico == null) {
            sistemaUnico = new Sistema();
        }
        return sistemaUnico;
    }

    public int getTiempoPromedio() {
        int tiempoPromedio = tiempoPreguntas / (cantidadPreguntas * 1000);
        return tiempoPromedio;
    }

    public int getCantidadRespuestasCorrectas() {
        return cantidadRespuestasCorrectas;
    }

    public int getCantidadPreguntas() {
        return cantidadPreguntas;
    }

    /**
     * *
     *
     * @return Retorna true si hay mas preguntas por cargar de la lista de
     * preguntas
     */
    public boolean hayMasPreguntas() {
        boolean ret;
        ret = !this.listaPreguntas.isEmpty();
        return ret;
    }

    public ArrayList<Pregunta> getListaPreguntas() {
        return listaPreguntas;
    }

    /**
     * *
     * Se borra todo el conenido guardado en las variables de clase para
     * sistema.
     */
    public void limpiarSistema() {
        this.listaPreguntas.clear();
        this.manejadorBarraProgreso.clear();
    }

    public void crearListaPreguntasVacia() {
        if (this.listaPreguntas == null) {
            listaPreguntas = new ArrayList<>();
        }
    }

    public void crearManejadorBarraProgresoVacio() {
        this.manejadorBarraProgreso = new ArrayList<>();
    }

    public void crearRegistroVacio() {
        this.registro = new ArrayList<>();
    }

    public ArrayList<Pregunta> getManejadorBarraProgreso() {
        return manejadorBarraProgreso;
    }

    public Pregunta siguientePregunta() {
        return this.listaPreguntas.get(0);
    }

    public void sumarContadorPregunta() {
        this.cantidadPreguntas++;
    }

    /**
     * *
     * Elimina la pregunta en la primer posicion del ArrayList de preguntas.
     */
    public void eliminarPrimerPreguntaCargada() {
        if (!this.listaPreguntas.isEmpty()) {
            this.listaPreguntas.remove(0);
        }
    }

    /**
     * *
     * Aumenta la variable de respuestas correctas en uno.
     */
    public void sumarContadorRespuestaCorrecta() {
        cantidadRespuestasCorrectas++;
    }

    /**
     * *
     * Agrega el resultado al arraylist de registro; en caso que este no este
     * inicializado lo inicializa.
     *
     * @param resultado el resultado a agregar.
     */
    public void agregarResultado(Resultado resultado) {
        if (registro == null) {
            registro = new ArrayList<>();
        }
        registro.add(resultado);
        tiempoPreguntas += resultado.getTiempo();
    }

    public ArrayList<Resultado> getRegistro() {
        return registro;
    }

    public void setListaPreguntas(ArrayList<Pregunta> listaPreguntas) {
        this.listaPreguntas = listaPreguntas;
    }

    public void setRegistro(ArrayList<Resultado> registro) {
        this.registro = registro;
    }

    public static void setSistemaUnico(Sistema sistemaUnico) {
        Sistema.sistemaUnico = sistemaUnico;
    }
}
