package uy.ort.edu.Obl2.dominio;

import java.util.ArrayList;
import java.util.Objects;

public class Pregunta {

    /**
     * *
     * Enum tipo puede tomar tres posibles valores: MULTIPLE, CORTA y
     * VERDADERO_FALSO
     */
    public enum Tipo {
        MULTIPLE, CORTA, VERDADERO_FALSO
    }

    private final String titulo;
    private final String pregunta;
    private final ArrayList<String> respuestas;
    private final String respuestaCorrecta;
    private final ArrayList<String> comentario;
    private final Tipo tipo;
    private final int tiempo;

    /**
     * *
     * Construye el objeto Pregunta basado en los parametros dados
     *
     * @param titulo el titulo que tendrá la pregunta
     * @param pregunta el contenido de la pregunta que tendrá
     * @param respuestas un ArrayList con las posibles respuestas
     * @param respuestaCorrecta un String que contiene la respuesta correcta
     * @param comentario si es opcion multiple se incluirá un comentario en
     * forma de String
     * @param tipo el Tipo que tendrá la pregunta; puede ser: MULTIPLE, CORTA,
     * VERDADERO_FALSO
     * @param tiempo el tiempo que tendrá la pregunta
     */
    public Pregunta(String titulo, String pregunta,
            ArrayList<String> respuestas, String respuestaCorrecta,
            ArrayList<String> comentario, Tipo tipo, int tiempo) {
        this.titulo = titulo;
        this.pregunta = pregunta;
        this.respuestas = respuestas;
        this.respuestaCorrecta = respuestaCorrecta;
        this.comentario = comentario;
        this.tipo = tipo;
        this.tiempo = tiempo;
    }

    public String getPregunta() {
        return this.pregunta;
    }

    public String getTitulo() {
        return titulo;
    }

    public ArrayList<String> getRespuestas() {
        return respuestas;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public ArrayList<String> getComentario() {
        return comentario;
    }

    public Tipo getTipo() {
        return tipo;
    }

    /**
     * *
     *
     * @param obj la otra Pregunta a comparar igualdad (funciona con otros
     * objetos)
     * @return true si son iguales
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pregunta other = (Pregunta) obj;
        if (this.tiempo != other.tiempo) {
            return false;
        }
        if (!Objects.equals(this.titulo, other.titulo)) {
            return false;
        }
        if (!Objects.equals(this.pregunta, other.pregunta)) {
            return false;
        }
        if (!Objects.equals(this.respuestaCorrecta, other.respuestaCorrecta)) {
            return false;
        }
        if (!Objects.equals(this.respuestas, other.respuestas)) {
            return false;
        }
        if (!Objects.equals(this.comentario, other.comentario)) {
            return false;
        }
        if (this.tipo != other.tipo) {
            return false;
        }
        return true;
    }

    public int getTiempo() {
        return tiempo;
    }

}
