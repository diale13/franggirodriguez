/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.interfaz;

import java.applet.AudioClip;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JRadioButton;
import uy.ort.edu.Obl2.auxiliar.Parser;
import uy.ort.edu.Obl2.dominio.Pregunta;
import uy.ort.edu.Obl2.dominio.Resultado;
import uy.ort.edu.Obl2.dominio.Sistema;

/**
 *
 * @author Franggi y Rodriguez
 */
public class OpcionMultiple extends javax.swing.JFrame {

    private Sistema sistemaGlobal;
    private Pregunta pregunta;
    private int tiempoactual;
    private AudioClip musicaRespuesta;
    private ArrayList<Pregunta> preguntasAux;
    private Timer timer;
    @SuppressWarnings("FieldMayBeFinal")
    private Parser parser;

    /**
     * *
     * Constructor de la ventana OpcionMultiple, recibe una pregunta sobre esa
     * pregunta se basa la creación de la ventana.
     *
     * @param pregunta Pregunta sobre la cual se basa la ventana
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public OpcionMultiple(Pregunta pregunta) {
        initComponents();
        timer = new Timer();
        this.setLocationRelativeTo(null);
        addButtons();
        this.labelCorrectoGif.setVisible(false);
        this.labelIncorrectoGif.setVisible(false);
        this.pregunta = pregunta;
        this.labelPregunta.setText(pregunta.getPregunta());
        this.labelTitulo.setText(pregunta.getTitulo());
        cargarPreguntas();
        borrarBotonesSobrantes();
        cargarComentarios();
        borrarComentariosSobrantes();
        this.sistemaGlobal = Sistema.obtenerSistemaGlobal();
        this.parser = sistemaGlobal.getParserGlobal();
        this.setResizable(false);
        this.tiempoactual = 0;
        manejadorCronometro();
        this.labelNoSelecciono.setVisible(false);
        this.preguntasAux = sistemaGlobal.getManejadorBarraProgreso();
        manejadorBarra();
        this.buttonResponder.setVisible(true);
        this.buttonSiguientePregunta.setVisible(false);
        this.labelAyuda1.setVisible(false);
        this.labelAyuda2.setVisible(false);
    }

    /**
     * *
     * Mediante este metodo se controla el estado de la barra
     */
    public void manejadorBarra() {
        this.barraProgreso.setMaximum(sistemaGlobal.getManejadorBarraProgreso().size());
        this.barraProgreso.setValue(findIndex(pregunta));
        this.barraProgreso.setForeground(Color.green);
        int numeroDePregunta = findIndex(pregunta) + 1;
        labelBarra.setText("Pregunta " + numeroDePregunta + " de " + preguntasAux.size());
        this.barraProgreso.setToolTipText("Vas " + findIndex(pregunta) + " preguntas de " + preguntasAux.size());
    }

    /**
     * *
     * Método auxiliar del manejador de barra, este busca la pregunta parametro
     * en el ArrayList auxiliar y retorna su indice.
     *
     * @param pregunta la pregunta cuya posicion se quiere buscar
     * @return la posicion de la pregunta en el ArrayList de preguntas original
     */
    public int findIndex(Pregunta pregunta) {
        int ret = preguntasAux.size();
        for (int i = 0; i < this.preguntasAux.size(); i++) {
            if (preguntasAux.get(i).equals(pregunta)) {
                ret = i;
            }
        }
        return ret;
    }

    /**
     * *
     * Método auxiliar que maneja el cronometro
     */
    public void manejadorCronometro() {
        TimerTask tarea = new TimerTask() {
            int tiempoPregunta = pregunta.getTiempo();

            @Override
            public void run() {
                int ok = 0;
                if (tiempoactual < tiempoPregunta * 1000) {
                    ok = 1;
                    tiempoactual = tiempoactual + 1000;
                }
                switch (ok) {
                    case 1:
                        int tiempoDisplay = tiempoactual / 1000;
                        labelTiempo.setText(Integer.toString(tiempoPregunta - tiempoDisplay));
                        break;
                    case 0:
                        labelTiempo.setText("SE ACABO EL TIEMPO");
                        labelIncorrectoGif.setVisible(true);
                        labelNoSelecciono.setText("Se terminó el tiempo");
                        labelNoSelecciono.setForeground(Color.red);
                        musicaRespuesta = java.applet.Applet.newAudioClip(getClass().getResource("/res/wrong.wav"));
                        musicaRespuesta.play();
                        buttonResponderActionPerformed(null);
                        this.cancel();
                        break;
                    default:
                        break;
                }

            }

        };
        timer.scheduleAtFixedRate(tarea, 0, 1000);
    }

    /**
     * *
     * Añade los botones al mismo grupo con el fin de que solo se pueda elegir
     * uno a la vez
     */
    private void addButtons() {
        this.buttonGroup1.add(respuesta1RadioButton);
        this.buttonGroup1.add(respuesta2RadioButton);
        this.buttonGroup1.add(respuesta3RadioButton);
        this.buttonGroup1.add(respuesta4RadioButton);
        this.buttonGroup1.add(respuesta5RadioButton);
        this.buttonGroup1.add(respuesta6RadioButton);
    }

    /**
     * *
     * Carga los botones de respuestas posibles con los textos adecuados
     */
    private void cargarPreguntas() {
        int largo = pregunta.getRespuestas().size();
        ArrayList<String> aux = (ArrayList<String>) pregunta.getRespuestas().clone();
        for (int i = 1; i <= largo; i++) {
            switch (i) {
                case 1:
                    respuesta1RadioButton.setText(aux.get(0));
                    respuesta1RadioButton.setVisible(true);
                    break;
                case 2:
                    respuesta2RadioButton.setText(aux.get(0));
                    respuesta2RadioButton.setVisible(true);
                    break;
                case 3:
                    respuesta3RadioButton.setText(aux.get(0));
                    respuesta3RadioButton.setVisible(true);
                    break;
                case 4:
                    respuesta4RadioButton.setText(aux.get(0));
                    respuesta4RadioButton.setVisible(true);
                    break;
                case 5:
                    respuesta5RadioButton.setText(aux.get(0));
                    respuesta5RadioButton.setVisible(true);
                    break;
                case 6:
                    respuesta6RadioButton.setText(aux.get(0));
                    respuesta6RadioButton.setVisible(true);
                    break;
                default:
                    break;
            }
            aux.remove(0);
        }
    }

    /**
     * *
     * Borra aquellos botones que no tienen pregunta cargada
     */
    private void borrarBotonesSobrantes() {
        int largo = pregunta.getRespuestas().size();
        for (int i = largo + 1; i <= 6; i++) {
            switch (i) {
                case 3:
                    respuesta3RadioButton.setVisible(false);
                    break;
                case 4:
                    respuesta4RadioButton.setVisible(false);
                    break;
                case 5:
                    respuesta5RadioButton.setVisible(false);
                    break;
                case 6:
                    respuesta6RadioButton.setVisible(false);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * *
     * Carga las labels de cada posible respuesta con los comentarios cargados
     * en sistema
     */
    private void cargarComentarios() {
        int largo = pregunta.getComentario().size();
        ArrayList<String> aux = (ArrayList<String>) pregunta.getComentario().clone();
        for (int i = 1; i <= largo; i++) {
            switch (i) {
                case 1:
                    labelComentario1.setText(aux.get(0));
                    labelComentario1.setVisible(false);
                    break;
                case 2:
                    labelComentario2.setText(aux.get(0));
                    labelComentario2.setVisible(false);
                    break;
                case 3:
                    labelComentario3.setText(aux.get(0));
                    labelComentario3.setVisible(false);
                    break;
                case 4:
                    labelComentario4.setText(aux.get(0));
                    labelComentario4.setVisible(false);
                    break;
                case 5:
                    labelComentario5.setText(aux.get(0));
                    labelComentario5.setVisible(false);
                    break;
                case 6:
                    labelComentario6.setText(aux.get(0));
                    labelComentario6.setVisible(false);
                    break;
                default:
                    break;
            }
            aux.remove(0);
        }
    }

    /**
     * *
     * Borra aquellos comentarios que no tienen preguntas cargada
     *
     */
    private void borrarComentariosSobrantes() {
        int largo = pregunta.getComentario().size();
        for (int i = largo + 1; i <= 6; i++) {
            switch (i) {
                case 3:
                    labelComentario3.setVisible(false);
                    break;
                case 4:
                    labelComentario4.setVisible(false);
                    break;
                case 5:
                    labelComentario5.setVisible(false);
                    break;
                case 6:
                    labelComentario6.setVisible(false);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * *
     *
     * @return retorna el JRadioButton seleccionado por el usuario, null si no
     * se eligió ninguno
     */
    JRadioButton seleccionado() {
        JRadioButton selected;
        if (this.respuesta1RadioButton.isSelected()) {
            selected = respuesta1RadioButton;
        } else if (this.respuesta2RadioButton.isSelected()) {
            selected = respuesta2RadioButton;
        } else if (this.respuesta3RadioButton.isSelected()) {
            selected = respuesta3RadioButton;
        } else if (this.respuesta4RadioButton.isSelected()) {
            selected = respuesta4RadioButton;
        } else if (this.respuesta5RadioButton.isSelected()) {
            selected = respuesta5RadioButton;
        } else if (this.respuesta6RadioButton.isSelected()) {
            selected = respuesta6RadioButton;
        } else {
            selected = null;
        }
        return selected;
    }

    /**
     * *
     * En caso de que el boton seleccionado fue correcto se muestra el gif y
     * sonido apropiado, asi como se pinta el JRadioButton apropiado
     *
     * @param selected el JRadioButton seleccionado
     */
    private void correcto(JRadioButton selected) {
        labelNoSelecciono.setVisible(false);
        labelCorrectoGif.setVisible(true);
        this.labelIncorrectoGif.setVisible(false);
        mostrarComentario(selected);
        selected.setForeground(Color.green);
        this.musicaRespuesta = java.applet.Applet.newAudioClip(getClass().getResource("/res/good.wav"));
        musicaRespuesta.play();
    }

    /**
     * *
     * En caso de que el boton seleccionado fue correcto se muestra el gif y
     * sonido apropiado, asi como se pinta el JRadioButton apropiado
     *
     * @param selected el JRadioButton seleccionado
     */
    private void incorrecto(JRadioButton selected) {
        this.labelCorrectoGif.setVisible(false);
        this.labelIncorrectoGif.setVisible(true);
        selected.setForeground(Color.red);
        mostrarComentario(selected);
        selected.setForeground(Color.red);
        this.musicaRespuesta = java.applet.Applet.newAudioClip(getClass().getResource("/res/wrong.wav"));
        musicaRespuesta.play();
    }

    /**
     * *
     * muestra el comentario de la pregunta cuyo boton fue seleccionado
     *
     * @param selected el JRadioButton seleccionado por el usuario
     */
    private void mostrarComentario(JRadioButton selected) {
        if (selected.equals(respuesta1RadioButton)) {
            labelComentario1.setVisible(true);
        } else if (selected.equals(respuesta2RadioButton)) {
            labelComentario2.setVisible(true);
        } else if (selected.equals(respuesta3RadioButton)) {
            labelComentario3.setVisible(true);
        } else if (selected.equals(respuesta4RadioButton)) {
            labelComentario4.setVisible(true);
        } else if (selected.equals(respuesta5RadioButton)) {
            labelComentario5.setVisible(true);
        } else if (selected.equals(respuesta6RadioButton)) {
            labelComentario6.setVisible(true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        labelTiempo = new javax.swing.JLabel();
        labelPregunta = new javax.swing.JLabel();
        respuesta1RadioButton = new javax.swing.JRadioButton();
        respuesta2RadioButton = new javax.swing.JRadioButton();
        respuesta3RadioButton = new javax.swing.JRadioButton();
        respuesta4RadioButton = new javax.swing.JRadioButton();
        respuesta5RadioButton = new javax.swing.JRadioButton();
        respuesta6RadioButton = new javax.swing.JRadioButton();
        buttonAyuda = new javax.swing.JButton();
        buttonResponder = new javax.swing.JButton();
        labelNoSelecciono = new javax.swing.JLabel();
        labelComentario1 = new javax.swing.JLabel();
        labelComentario2 = new javax.swing.JLabel();
        labelComentario3 = new javax.swing.JLabel();
        labelComentario4 = new javax.swing.JLabel();
        labelComentario5 = new javax.swing.JLabel();
        labelComentario6 = new javax.swing.JLabel();
        labelIncorrectoGif = new javax.swing.JLabel();
        labelCorrectoGif = new javax.swing.JLabel();
        labelTitulo = new javax.swing.JLabel();
        barraProgreso = new javax.swing.JProgressBar();
        labelBarra = new javax.swing.JLabel();
        buttonSiguientePregunta = new javax.swing.JButton();
        labelAyuda1 = new javax.swing.JLabel();
        labelAyuda2 = new javax.swing.JLabel();
        buttonMenu = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(0, 102, 102));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 720, -1, -1));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labelTiempo.setFont(new java.awt.Font("Dialog", 1, 28)); // NOI18N
        labelTiempo.setForeground(new java.awt.Color(0, 102, 102));
        labelTiempo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        labelTiempo.setText("30'");
        jPanel5.add(labelTiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 30, 340, 30));

        labelPregunta.setForeground(new java.awt.Color(0, 0, 0));
        labelPregunta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPregunta.setText("OPCION MULTIPLE");
        jPanel5.add(labelPregunta, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 90, 690, 20));

        respuesta1RadioButton.setBackground(new java.awt.Color(255, 255, 255));
        respuesta1RadioButton.setForeground(new java.awt.Color(0, 0, 0));
        respuesta1RadioButton.setText("jRadioButton1");
        respuesta1RadioButton.setFocusPainted(false);
        jPanel5.add(respuesta1RadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, 680, -1));

        respuesta2RadioButton.setBackground(new java.awt.Color(255, 255, 255));
        respuesta2RadioButton.setForeground(new java.awt.Color(0, 0, 0));
        respuesta2RadioButton.setText("jRadioButton2");
        respuesta2RadioButton.setFocusPainted(false);
        jPanel5.add(respuesta2RadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 190, 680, -1));

        respuesta3RadioButton.setBackground(new java.awt.Color(255, 255, 255));
        respuesta3RadioButton.setForeground(new java.awt.Color(0, 0, 0));
        respuesta3RadioButton.setText("jRadioButton3");
        respuesta3RadioButton.setFocusPainted(false);
        jPanel5.add(respuesta3RadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 250, 680, -1));

        respuesta4RadioButton.setBackground(new java.awt.Color(255, 255, 255));
        respuesta4RadioButton.setForeground(new java.awt.Color(0, 0, 0));
        respuesta4RadioButton.setText("jRadioButton4");
        respuesta4RadioButton.setFocusPainted(false);
        jPanel5.add(respuesta4RadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 310, 680, -1));

        respuesta5RadioButton.setBackground(new java.awt.Color(255, 255, 255));
        respuesta5RadioButton.setForeground(new java.awt.Color(0, 0, 0));
        respuesta5RadioButton.setText("jRadioButton5");
        respuesta5RadioButton.setFocusPainted(false);
        jPanel5.add(respuesta5RadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 370, 680, -1));

        respuesta6RadioButton.setBackground(new java.awt.Color(255, 255, 255));
        respuesta6RadioButton.setForeground(new java.awt.Color(0, 0, 0));
        respuesta6RadioButton.setText("jRadioButton6");
        respuesta6RadioButton.setFocusPainted(false);
        jPanel5.add(respuesta6RadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 430, 680, -1));

        buttonAyuda.setBackground(new java.awt.Color(0, 102, 102));
        buttonAyuda.setForeground(new java.awt.Color(255, 255, 255));
        buttonAyuda.setText("Ayuda");
        buttonAyuda.setToolTipText("");
        buttonAyuda.setFocusPainted(false);
        buttonAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAyudaActionPerformed(evt);
            }
        });
        jPanel5.add(buttonAyuda, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 640, -1, -1));

        buttonResponder.setBackground(new java.awt.Color(0, 102, 102));
        buttonResponder.setForeground(new java.awt.Color(255, 255, 255));
        buttonResponder.setText("Responder");
        buttonResponder.setFocusPainted(false);
        buttonResponder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonResponderActionPerformed(evt);
            }
        });
        jPanel5.add(buttonResponder, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 640, 170, -1));

        labelNoSelecciono.setForeground(new java.awt.Color(0, 0, 0));
        labelNoSelecciono.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelNoSelecciono.setText("¡Debe seleccionar una opción!");
        jPanel5.add(labelNoSelecciono, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 620, 190, 20));

        labelComentario1.setText("jLabel2");
        jPanel5.add(labelComentario1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 160, 680, 30));

        labelComentario2.setText("jLabel2");
        jPanel5.add(labelComentario2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 220, 680, 30));

        labelComentario3.setText("jLabel2");
        jPanel5.add(labelComentario3, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 280, 680, 30));

        labelComentario4.setText("jLabel2");
        jPanel5.add(labelComentario4, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 340, 680, 30));

        labelComentario5.setText("jLabel2");
        jPanel5.add(labelComentario5, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 400, 680, 30));

        labelComentario6.setText("jLabel2");
        jPanel5.add(labelComentario6, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 460, 680, 30));

        labelIncorrectoGif.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uy/ort/edu/Obl2/recursos/sad.gif"))); // NOI18N
        jPanel5.add(labelIncorrectoGif, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 490, 360, 220));

        labelCorrectoGif.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uy/ort/edu/Obl2/recursos/success.gif"))); // NOI18N
        jPanel5.add(labelCorrectoGif, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 470, 230, 250));

        labelTitulo.setForeground(new java.awt.Color(0, 0, 0));
        labelTitulo.setText("Título");
        jPanel5.add(labelTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 70, 690, -1));

        barraProgreso.setBackground(new java.awt.Color(0, 102, 102));
        barraProgreso.setForeground(new java.awt.Color(0, 102, 102));
        barraProgreso.setOpaque(false);
        jPanel5.add(barraProgreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 40, 410, 20));

        labelBarra.setForeground(new java.awt.Color(0, 102, 102));
        labelBarra.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBarra.setText("Label barra");
        jPanel5.add(labelBarra, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 20, 410, -1));

        buttonSiguientePregunta.setBackground(new java.awt.Color(0, 102, 102));
        buttonSiguientePregunta.setForeground(new java.awt.Color(255, 255, 255));
        buttonSiguientePregunta.setText("Siguiente pregunta");
        buttonSiguientePregunta.setFocusPainted(false);
        buttonSiguientePregunta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSiguientePreguntaActionPerformed(evt);
            }
        });
        jPanel5.add(buttonSiguientePregunta, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 640, 170, -1));

        labelAyuda1.setBackground(new java.awt.Color(0, 0, 0));
        labelAyuda1.setForeground(new java.awt.Color(0, 0, 0));
        labelAyuda1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelAyuda1.setText("Elija la opción que crea correcta y ");
        jPanel5.add(labelAyuda1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 600, 200, 20));

        labelAyuda2.setForeground(new java.awt.Color(0, 0, 0));
        labelAyuda2.setText(" luego presione el botón \"Responder\"");
        jPanel5.add(labelAyuda2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 620, -1, -1));

        buttonMenu.setBackground(new java.awt.Color(0, 102, 102));
        buttonMenu.setForeground(new java.awt.Color(255, 255, 255));
        buttonMenu.setText("Menú");
        buttonMenu.setFocusPainted(false);
        buttonMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonMenuActionPerformed(evt);
            }
        });
        jPanel5.add(buttonMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, -1, -1));

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1080, 720));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * *
     * Al presionar el botón de ayuda se muestran los labels correspondientes
     *
     * @param evt boton ayuda presionado
     */
    private void buttonAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAyudaActionPerformed
        this.labelAyuda1.setVisible(true);
        this.labelAyuda2.setVisible(true);
    }//GEN-LAST:event_buttonAyudaActionPerformed

    /**
     * *
     * Se consulta al usuario si quiere volver al menú principal y se actua
     * acorde a la eleccion
     *
     * @param evt botón menú presionado
     */
    private void buttonMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonMenuActionPerformed
        parser.volverAlMenu(this);
    }//GEN-LAST:event_buttonMenuActionPerformed

    /**
     * *
     * Se efectua intento de respuesta, si no se eligió ninguna opcion se avisa
     * al usuario, caso contrario actua acorde
     *
     * @param evt botón responder accionado
     */
    private void buttonResponderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonResponderActionPerformed
        this.labelAyuda1.setVisible(false);
        this.labelAyuda2.setVisible(false);
        String respuestaSelecionada = seleccionado().getText();
        String respuestaCorrecta = pregunta.getRespuestaCorrecta();
        if (labelTiempo.getText().equals("SE ACABO EL TIEMPO")) {
            Resultado nuevoResultado = new Resultado(pregunta.getTiempo() * 1000,
                    pregunta.getPregunta(), respuestaCorrecta,
                    " ", false);
            sistemaGlobal.agregarResultado(nuevoResultado);
            this.labelIncorrectoGif.setVisible(true);
            this.buttonResponder.setVisible(false);
            this.buttonSiguientePregunta.setVisible(true);
        } else {
            if (seleccionado() == null) {
                this.labelNoSelecciono.setText("¡Debe seleccionar una opción!");
                this.labelNoSelecciono.setForeground(Color.red);
                this.labelNoSelecciono.setVisible(true);
                return;
            } else {
                timer.cancel();
                boolean esCorrecto = true;
                if (respuestaSelecionada.equals(respuestaCorrecta)) {
                    sistemaGlobal.sumarContadorRespuestaCorrecta();
                    correcto(seleccionado());
                } else {
                    incorrecto(seleccionado());
                    esCorrecto = false;
                }
                Resultado nuevoResultado = new Resultado(tiempoactual,
                        pregunta.getPregunta(), respuestaCorrecta,
                        respuestaSelecionada, esCorrecto);
                sistemaGlobal.agregarResultado(nuevoResultado);
                this.buttonResponder.setVisible(false);
                this.buttonSiguientePregunta.setVisible(true);
                this.getRootPane().setDefaultButton(buttonResponder);
            }
        }
    }//GEN-LAST:event_buttonResponderActionPerformed

    /**
     * *
     * De ser posible se avanza a la siguiente ventana usando
     * pasarAlaSiguientePregunta de sistema. Si no es posible invoca
     * VentanaFinalRegistro
     *
     * @see Sistema pasarAlaSiguientePregunta
     *
     * @param evt botón siguiente pregunta accionado
     */
    private void buttonSiguientePreguntaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSiguientePreguntaActionPerformed
        if (sistemaGlobal.hayMasPreguntas()) {
            parser.pasarAlaSiguientePregunta(this, sistemaGlobal.siguientePregunta());
        } else {
            VentanaFinalRegistro fin = new VentanaFinalRegistro();
            this.setVisible(false);
            fin.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_buttonSiguientePreguntaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraProgreso;
    private javax.swing.JButton buttonAyuda;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton buttonMenu;
    private javax.swing.JButton buttonResponder;
    private javax.swing.JButton buttonSiguientePregunta;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel labelAyuda1;
    private javax.swing.JLabel labelAyuda2;
    private javax.swing.JLabel labelBarra;
    private javax.swing.JLabel labelComentario1;
    private javax.swing.JLabel labelComentario2;
    private javax.swing.JLabel labelComentario3;
    private javax.swing.JLabel labelComentario4;
    private javax.swing.JLabel labelComentario5;
    private javax.swing.JLabel labelComentario6;
    private javax.swing.JLabel labelCorrectoGif;
    private javax.swing.JLabel labelIncorrectoGif;
    private javax.swing.JLabel labelNoSelecciono;
    private javax.swing.JLabel labelPregunta;
    private javax.swing.JLabel labelTiempo;
    private javax.swing.JLabel labelTitulo;
    private javax.swing.JRadioButton respuesta1RadioButton;
    private javax.swing.JRadioButton respuesta2RadioButton;
    private javax.swing.JRadioButton respuesta3RadioButton;
    private javax.swing.JRadioButton respuesta4RadioButton;
    private javax.swing.JRadioButton respuesta5RadioButton;
    private javax.swing.JRadioButton respuesta6RadioButton;
    // End of variables declaration//GEN-END:variables
}
