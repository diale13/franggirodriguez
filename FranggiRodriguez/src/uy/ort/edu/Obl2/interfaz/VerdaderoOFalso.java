/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.interfaz;

import java.applet.AudioClip;
import java.awt.Color;
import java.util.ArrayList;

import uy.ort.edu.Obl2.dominio.Pregunta;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import uy.ort.edu.Obl2.auxiliar.Parser;
import uy.ort.edu.Obl2.dominio.Sistema;
import uy.ort.edu.Obl2.dominio.Resultado;

/**
 *
 * @author Franggi y Rodriguez
 */
public class VerdaderoOFalso extends javax.swing.JFrame {

    private ArrayList<Pregunta> preguntasAux;
    private AudioClip musicaRespuesta;
    private Sistema sistema;
    private Pregunta pregunta;
    private int tiempoactual;
    private final Timer timer = new Timer();
    private Parser parser;

    /**
     * Creates new form VerdaderoOFalso
     */
    public VerdaderoOFalso(Pregunta pregunta) {
        initComponents();
        cargarVentana(pregunta);
        this.getRootPane().setDefaultButton(buttonResponder);
    }

    /**
     * *
     * Carga la ventana VerdaderoOFalso acorde a la pregunta parametro
     *
     * @param pregunta pregunta sobre la cual se basa la clase
     */
    public void cargarVentana(Pregunta pregunta) {
        tituloLabel.setText(pregunta.getTitulo());
        labelPregunta.setText(pregunta.getPregunta());
        ButtonGroup grupoVerdaderoOfalso = new ButtonGroup();
        grupoVerdaderoOfalso.add(verdaderoRadioButton);
        grupoVerdaderoOfalso.add(falsoRadioButton);
        this.setLocationRelativeTo(null);
        this.pregunta = pregunta;
        this.setResizable(false);
        tituloLabel.setText(pregunta.getTitulo());
        labelPregunta.setText(pregunta.getPregunta());
        noSeleccionoLabel.setVisible(false);
        this.buttonGroup1.add(falsoRadioButton);
        this.buttonGroup1.add(verdaderoRadioButton);
        this.labelCorrectoGif.setVisible(false);
        this.labelIncorrectoGif.setVisible(false);
        this.sistema = Sistema.obtenerSistemaGlobal();
        this.parser = sistema.getParserGlobal();
        tiempoactual = 0;
        manejadorCronometro();
        this.preguntasAux = sistema.getManejadorBarraProgreso();
        this.buttonResponder.setVisible(true);
        this.buttonSiguientePregunta.setVisible(false);
        this.labelAyuda1.setVisible(false);
        this.labelAyuda2.setVisible(false);
        this.getRootPane().setDefaultButton(buttonSiguientePregunta);
        manejadorBarra();
    }

    /**
     * *
     * Mediante este metodo se controla el estado de la barra
     */
    public void manejadorBarra() {
        this.barraProgreso.setMaximum(sistema.getManejadorBarraProgreso().size());
        this.barraProgreso.setValue(findIndex(pregunta));
        this.barraProgreso.setForeground(Color.green);
        int numeroDePregunta = findIndex(pregunta) + 1;
        labelBarra.setText("Pregunta " + numeroDePregunta + " de " + preguntasAux.size());
        this.barraProgreso.setToolTipText("Vas " + findIndex(pregunta) + " preguntas de " + preguntasAux.size());
    }

    /**
     * *
     * Método auxiliar del manejador de barra, este busca la pregunta parametro
     * en el ArrayList auxiliar y retorna su indice.
     *
     * @param pregunta la pregunta cuya posicion se quiere buscar
     * @return la posicion de la pregunta en el ArrayList de preguntas original
     */
    public int findIndex(Pregunta pregunta) {
        int ret = preguntasAux.size();
        for (int i = 0; i < this.preguntasAux.size(); i++) {
            if (preguntasAux.get(i).equals(pregunta)) {
                ret = i;
            }
        }
        return ret;
    }

    /**
     * *
     * Método auxiliar que maneja el cronometro
     */
    public void manejadorCronometro() {
        TimerTask tarea = new TimerTask() {
            int tiempoPregunta = pregunta.getTiempo();

            @Override
            public void run() {
                int ok = 0;
                if (tiempoactual < tiempoPregunta * 1000) {
                    ok = 1;
                    tiempoactual = tiempoactual + 1000;
                }
                switch (ok) {
                    case 1:
                        int tiempoDisplay = tiempoactual / 1000;
                        labelTiempo.setText(Integer.toString(tiempoPregunta - tiempoDisplay));
                        break;
                    case 0:
                        labelTiempo.setText("SE ACABO EL TIEMPO");
                        labelIncorrectoGif.setVisible(true);
                        noSeleccionoLabel.setText("Se terminó el tiempo");
                        noSeleccionoLabel.setForeground(Color.red);
                        musicaRespuesta = java.applet.Applet.newAudioClip(getClass().getResource("/res/wrong.wav"));
                        musicaRespuesta.play();
                        buttonResponderActionPerformed(null);
                        this.cancel();

                        break;
                    default:
                        break;
                }

            }

        };
        timer.scheduleAtFixedRate(tarea, 0, 1000);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        tituloLabel = new javax.swing.JLabel();
        labelTiempo = new javax.swing.JLabel();
        buttonAyuda = new javax.swing.JButton();
        buttonSiguientePregunta = new javax.swing.JButton();
        verdaderoRadioButton = new javax.swing.JRadioButton();
        falsoRadioButton = new javax.swing.JRadioButton();
        barraProgreso = new javax.swing.JProgressBar();
        noSeleccionoLabel = new javax.swing.JLabel();
        labelCorrectoGif = new javax.swing.JLabel();
        labelIncorrectoGif = new javax.swing.JLabel();
        labelBarra = new javax.swing.JLabel();
        labelPregunta = new javax.swing.JLabel();
        buttonResponder = new javax.swing.JButton();
        labelAyuda1 = new javax.swing.JLabel();
        labelAyuda2 = new javax.swing.JLabel();
        buttonMenu = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(424, 300));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tituloLabel.setBackground(new java.awt.Color(0, 102, 102));
        tituloLabel.setForeground(new java.awt.Color(0, 0, 0));
        tituloLabel.setText("título a cargar");
        jPanel1.add(tituloLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 90, 720, -1));

        labelTiempo.setBackground(new java.awt.Color(0, 102, 102));
        labelTiempo.setFont(new java.awt.Font("Dialog", 1, 28)); // NOI18N
        labelTiempo.setForeground(new java.awt.Color(0, 102, 102));
        labelTiempo.setText("30'");
        jPanel1.add(labelTiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 30, 340, 30));

        buttonAyuda.setBackground(new java.awt.Color(0, 102, 102));
        buttonAyuda.setForeground(new java.awt.Color(255, 255, 255));
        buttonAyuda.setText("Ayuda");
        buttonAyuda.setFocusPainted(false);
        buttonAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAyudaActionPerformed(evt);
            }
        });
        jPanel1.add(buttonAyuda, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 640, 170, -1));

        buttonSiguientePregunta.setBackground(new java.awt.Color(0, 102, 102));
        buttonSiguientePregunta.setForeground(new java.awt.Color(255, 255, 255));
        buttonSiguientePregunta.setText("Siguiente pregunta");
        buttonSiguientePregunta.setFocusPainted(false);
        buttonSiguientePregunta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSiguientePreguntaActionPerformed(evt);
            }
        });
        jPanel1.add(buttonSiguientePregunta, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 640, 170, -1));

        verdaderoRadioButton.setBackground(new java.awt.Color(255, 255, 255));
        verdaderoRadioButton.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        verdaderoRadioButton.setForeground(new java.awt.Color(0, 0, 0));
        verdaderoRadioButton.setText("Verdadero");
        verdaderoRadioButton.setFocusPainted(false);
        jPanel1.add(verdaderoRadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 270, 170, 40));

        falsoRadioButton.setBackground(new java.awt.Color(255, 255, 255));
        falsoRadioButton.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        falsoRadioButton.setForeground(new java.awt.Color(0, 0, 0));
        falsoRadioButton.setText("Falso");
        falsoRadioButton.setFocusPainted(false);
        jPanel1.add(falsoRadioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 270, 170, 40));

        barraProgreso.setBackground(new java.awt.Color(0, 102, 102));
        barraProgreso.setForeground(new java.awt.Color(0, 102, 102));
        barraProgreso.setOpaque(false);
        jPanel1.add(barraProgreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 40, 410, 20));

        noSeleccionoLabel.setForeground(new java.awt.Color(0, 0, 0));
        noSeleccionoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        noSeleccionoLabel.setText("¡Debe seleccionar verdadero o falso!");
        jPanel1.add(noSeleccionoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 620, 230, 20));

        labelCorrectoGif.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uy/ort/edu/Obl2/recursos/success.gif"))); // NOI18N
        jPanel1.add(labelCorrectoGif, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 390, 290, 220));

        labelIncorrectoGif.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uy/ort/edu/Obl2/recursos/sad.gif"))); // NOI18N
        jPanel1.add(labelIncorrectoGif, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 390, 360, 220));

        labelBarra.setForeground(new java.awt.Color(0, 0, 0));
        labelBarra.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBarra.setText("Label barra");
        jPanel1.add(labelBarra, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 20, 410, -1));

        labelPregunta.setBackground(new java.awt.Color(0, 102, 102));
        labelPregunta.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        labelPregunta.setForeground(new java.awt.Color(0, 0, 0));
        labelPregunta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPregunta.setText("pregunta a cargar");
        jPanel1.add(labelPregunta, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 140, 720, 30));

        buttonResponder.setBackground(new java.awt.Color(0, 102, 102));
        buttonResponder.setForeground(new java.awt.Color(255, 255, 255));
        buttonResponder.setText("Responder");
        buttonResponder.setFocusPainted(false);
        buttonResponder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonResponderActionPerformed(evt);
            }
        });
        jPanel1.add(buttonResponder, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 640, 170, -1));

        labelAyuda1.setBackground(new java.awt.Color(0, 0, 0));
        labelAyuda1.setForeground(new java.awt.Color(0, 0, 0));
        labelAyuda1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelAyuda1.setText("Elija la opción que crea correcta y ");
        jPanel1.add(labelAyuda1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 600, 200, 20));

        labelAyuda2.setForeground(new java.awt.Color(0, 0, 0));
        labelAyuda2.setText(" luego presione el botón \"Responder\"");
        jPanel1.add(labelAyuda2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 620, -1, -1));

        buttonMenu.setBackground(new java.awt.Color(0, 102, 102));
        buttonMenu.setForeground(new java.awt.Color(255, 255, 255));
        buttonMenu.setText("Menú");
        buttonMenu.setFocusPainted(false);
        buttonMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonMenuActionPerformed(evt);
            }
        });
        jPanel1.add(buttonMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1080, 720));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * *
     * Al presionar el botón de ayuda se muestran los labels correspondientes
     *
     * @param evt boton ayuda presionado
     */
    private void buttonAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAyudaActionPerformed
        this.labelAyuda1.setVisible(true);
        this.labelAyuda2.setVisible(true);
    }//GEN-LAST:event_buttonAyudaActionPerformed

    /**
     * *
     *
     * @return retorna el JRadioButton que fue seleccionado
     */
    private JRadioButton selectedRadioButton() {
        JRadioButton selected;
        if (verdaderoRadioButton.isSelected()) {
            selected = verdaderoRadioButton;
        } else {
            selected = falsoRadioButton;
        }
        return selected;
    }

    /**
     * *
     *
     * @param selected el JRadioButton seleccionado por el usuario
     * @return T si el boton seleccionado es el de verdadero,F en otro caso
     */
    private String letraVoF(JRadioButton selected) {
        String retorno;
        if (selected.getText().equals("Verdadero")) {
            retorno = "T";
        } else {
            retorno = "F";
        }
        return retorno;
    }

    /**
     * *
     * En caso de que el boton seleccionado fue correcto se muestra el gif y
     * sonido apropiado, asi como se pinta el JRadioButton apropiado
     *
     * @param selected el JRadioButton seleccionado
     */
    private void correcto(JRadioButton selected) {
        selected.setForeground(Color.green);
        noSeleccionoLabel.setVisible(false);
        this.labelIncorrectoGif.setVisible(false);
        labelCorrectoGif.setVisible(true);
        this.labelCorrectoGif.repaint();
        this.repaint();
        this.musicaRespuesta = java.applet.Applet.newAudioClip(getClass().getResource("/res/good.wav"));
        musicaRespuesta.play();
    }

    /**
     * *
     * En caso de que el boton seleccionado fue correcto se muestra el gif y
     * sonido apropiado, asi como se pinta el JRadioButton apropiado
     *
     * @param selected el JRadioButton seleccionado
     */
    private void incorrecto(JRadioButton selected) {
        this.labelCorrectoGif.setVisible(false);
        noSeleccionoLabel.setVisible(false);
        this.labelIncorrectoGif.setVisible(true);
        labelIncorrectoGif.repaint();
        selected.setForeground(Color.red);
        this.musicaRespuesta = java.applet.Applet.newAudioClip(getClass().getResource("/res/wrong.wav"));
        musicaRespuesta.play();
    }

    /**
     * *
     * De ser posible se avanza a la siguiente ventana usando
     * pasarAlaSiguientePregunta de sistema. Si no es posible invoca
     * VentanaFinalRegistro
     *
     * @see Sistema pasarAlaSiguientePregunta
     *
     * @param evt botón siguiente pregunta accionado
     */

    private void buttonSiguientePreguntaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSiguientePreguntaActionPerformed
        if (sistema.hayMasPreguntas()) {
            parser.pasarAlaSiguientePregunta(this, sistema.siguientePregunta());
            this.dispose();
        } else {
            VentanaFinalRegistro fin = new VentanaFinalRegistro();
            this.setVisible(false);
            fin.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_buttonSiguientePreguntaActionPerformed

    /**
     * *
     * Se consulta al usuario si quiere volver al menú principal y se actua
     * acorde a la eleccion
     *
     * @param evt botón menú presionado
     */
    private void buttonMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonMenuActionPerformed
        parser.volverAlMenu(this);
    }//GEN-LAST:event_buttonMenuActionPerformed

    /**
     * *
     * Se efectua intento de respuesta, si no se eligió ninguna opcion se avisa
     * al usuario, caso contrario actua acorde
     *
     * @param evt botón responder accionado
     */
    private void buttonResponderActionPerformed(java.awt.event.ActionEvent evt) {
        this.labelAyuda1.setVisible(false);
        this.labelAyuda2.setVisible(false);
        String respuestaCorrecta = pregunta.getRespuestaCorrecta();
        String eligioVoF = letraVoF(selectedRadioButton());
        if (labelTiempo.getText().equals("SE ACABO EL TIEMPO")) {
            timer.cancel();
            Resultado nuevoResultado = new Resultado(pregunta.getTiempo() * 1000,
                    pregunta.getPregunta(), respuestaCorrecta,
                    " ", false);
            sistema.agregarResultado(nuevoResultado);
            this.labelIncorrectoGif.setVisible(true);
            this.buttonSiguientePregunta.setVisible(true);
            this.buttonResponder.setVisible(false);
        } else {
            if (!falsoRadioButton.isSelected() && !verdaderoRadioButton.isSelected()) {
                this.noSeleccionoLabel.setText("¡Debe seleccionar verdadero o falso!");
                this.noSeleccionoLabel.setForeground(Color.red);
                this.noSeleccionoLabel.setVisible(true);
            } else {
                timer.cancel();
                boolean esCorrecta = true;
                if (respuestaCorrecta.equals(eligioVoF)) {
                    correcto(selectedRadioButton());
                    sistema.sumarContadorRespuestaCorrecta();
                } else {
                    incorrecto(selectedRadioButton());
                    esCorrecta = false;

                }
                Resultado nuevoResultado = new Resultado(tiempoactual,
                        pregunta.getPregunta(), respuestaCorrecta,
                        eligioVoF, esCorrecta);
                sistema.agregarResultado(nuevoResultado);
                this.buttonResponder.setVisible(false);
                this.buttonSiguientePregunta.setVisible(true);
                this.getRootPane().setDefaultButton(buttonSiguientePregunta);
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraProgreso;
    private javax.swing.JButton buttonAyuda;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton buttonMenu;
    private javax.swing.JButton buttonResponder;
    private javax.swing.JButton buttonSiguientePregunta;
    private javax.swing.JRadioButton falsoRadioButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelAyuda1;
    private javax.swing.JLabel labelAyuda2;
    private javax.swing.JLabel labelBarra;
    private javax.swing.JLabel labelCorrectoGif;
    private javax.swing.JLabel labelIncorrectoGif;
    private javax.swing.JLabel labelPregunta;
    private javax.swing.JLabel labelTiempo;
    private javax.swing.JLabel noSeleccionoLabel;
    private javax.swing.JLabel tituloLabel;
    private javax.swing.JRadioButton verdaderoRadioButton;
    // End of variables declaration//GEN-END:variables
}
