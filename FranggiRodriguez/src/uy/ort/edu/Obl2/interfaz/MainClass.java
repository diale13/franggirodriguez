package uy.ort.edu.Obl2.interfaz;

import uy.ort.edu.Obl2.exception.ExepcionesGift;

public class MainClass {

    /**
     * *
     *
     * @param args
     * @throws ClassNotFoundException en caso de no encontrar la clase
     * @throws ExepcionesGift en caso de cargar mal el archivo gift lanzara una
     * exepcion apropiada
     */
    public static void main(String[] args) throws ClassNotFoundException, ExepcionesGift {
        try {
            MenuPrincipal menu = new MenuPrincipal();
            menu.setVisible(true);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
