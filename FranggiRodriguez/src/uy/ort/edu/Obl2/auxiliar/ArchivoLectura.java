package uy.ort.edu.Obl2.auxiliar;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * *
 *
 * @author Franggi y Rodriguez
 */
public class ArchivoLectura {

    private String linea;

    private Scanner in;

    /**
     * *
     * Construye un archivo lectura a partir del ubicado en la ruta pasada por
     * parametro
     *
     * @param archivo ubicacion del archivo a leer
     */
    public ArchivoLectura(String archivo) {
        try {
            in = new Scanner(Paths.get(archivo));
        } catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }

    }

    /**
     * *
     *
     * @return retorna true si hay mas lineas en el archivo a leer y avanza a la
     * siguiente
     */
    public boolean hayMasLineas() {
        boolean hay = false;
        if (in.hasNext()) {
            linea = in.nextLine();
            hay = true;
        }

        return hay;
    }

    /**
     * *
     *
     * @return retorna true si hay mas lineas sin avanzar en el archivo a leer
     * pero no avanza a la siguiente
     */
    public boolean hayMasLineasSinAvanzar() {
        boolean hay = false;
        if (in.hasNext()) {
            hay = true;
        }

        return hay;
    }

    /**
     * *
     * avanza a la siguiente linea
     */
    public void siguienteLinea() {
        linea = in.nextLine();
    }

    /**
     * *
     *
     * @return retorna el caracter de la siguiente linea en la posicion 0
     */
    public char siguienteLineaCharAt0() {
        linea = in.nextLine();
        return linea.charAt(0);
    }

    /**
     * *
     * @return retorna la linea que se esta leyendo
     */
    public String linea() {
        return linea;
    }

    /**
     * *
     * cierra el archivo lectura
     */
    public void cerrar() {
        in.close();
    }
}
