/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.auxiliar;

import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import uy.ort.edu.Obl2.dominio.Pregunta;
import uy.ort.edu.Obl2.dominio.Sistema;
import uy.ort.edu.Obl2.exception.ExepcionesGift;
import uy.ort.edu.Obl2.interfaz.MenuPrincipal;
import uy.ort.edu.Obl2.interfaz.OpcionMultiple;
import uy.ort.edu.Obl2.interfaz.RespuestaCorta;
import uy.ort.edu.Obl2.interfaz.VerdaderoOFalso;

/**
 *
 * @author Romina
 */
public class Parser {

    private Sistema sistemaGlobal;
    private String titulo;
    private String pregunta;
    private ArrayList<String> respuestasPosibles;
    private String respuestaCorrecta;
    private ArrayList<String> comentarios;
    private Pregunta.Tipo tipo;
    private int tiempo;
    private int lineaError;
    private ArchivoLectura aux;
    private String in;

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public void setRespuestasPosibles(ArrayList<String> respuestasPosibles) {
        this.respuestasPosibles = respuestasPosibles;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public void setComentarios(ArrayList<String> comentarios) {
        this.comentarios = comentarios;
    }

    public void setTipo(Pregunta.Tipo tipo) {
        this.tipo = tipo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public void setLineaError(int lineaError) {
        this.lineaError = lineaError;
    }

    public void setAux(ArchivoLectura aux) {
        this.aux = aux;
    }

    public void setIn(String in) {
        this.in = in;
    }

    /**
     * *
     * Carga el archivo gift y lo convierte en preguntas dentro del sistema.
     *
     * @param archivo recibe la ubicacion absoluta del archivo a cargar
     * @throws ExepcionesGift En caso de no cumplir adecuadamente el formato
     * gift se lanzara una exepcion que informara al usuario del error apropiado
     */
    public void cargarPreguntas(String archivo) throws ExepcionesGift {
        System.out.println("Por inicializar sistema");
        inicializarSistema(archivo);
        System.out.println("Inicialice sistema");
        while (aux.hayMasLineasSinAvanzar()) {
            System.out.println("ryjfgn");
            inicializarPregunta();
            System.out.println("inicialice pregunta");
            String tiempoString = cargarTiempo();
            validarTiempo(tiempoString);
            System.out.println("valide tiempo");
            avanzarEnArchivo();
            System.out.println("avance en archivo");
            validarTitulo();
            int cont = 2;
            cont = cargarValidarTitulo(cont);
            System.out.println("valide y cargue titulo");
            validarDosPuntosEntreTituloPregunta(cont);
            cont += 2;
            cargarPreguntaDelArchivo(cont);
            validarPregunta();
            System.out.println("valide y cargue pregunta");
            avanzarEnArchivo();
            validarComienzoLlave();
            avanzarEnArchivo();
            validarAsignarTipo();
            System.out.println("valide tipo");
            avanzarEnArchivo();
            cargarPregunta();
        }
    }

    /**
     * *
     * Pasa a la siguiente pregunta desde una ventana dada creando la siguiente.
     *
     * @param actual es la ventana en donde se encuentra y se quiere pasar a la
     * siguiente pregunta.
     * @param primerPregunta es la siguiente pregunta a cargar, no puede ser
     * null
     */
    public void pasarAlaSiguientePregunta(JFrame actual, Pregunta primerPregunta) {
        Pregunta.Tipo tipoPregunta = primerPregunta.getTipo();
        switch (tipoPregunta) {
            case VERDADERO_FALSO:
                VerdaderoOFalso ventanaVoF = new VerdaderoOFalso(primerPregunta);
                actual.setVisible(false);
                ventanaVoF.setVisible(true);
                sistemaGlobal.eliminarPrimerPreguntaCargada();
                actual.dispose();
                break;
            case MULTIPLE:
                OpcionMultiple ventanaMultiple = new OpcionMultiple(primerPregunta);
                actual.setVisible(false);
                ventanaMultiple.setVisible(true);
                sistemaGlobal.eliminarPrimerPreguntaCargada();
                actual.dispose();
                break;
            case CORTA:
                RespuestaCorta respuestaCorta = new RespuestaCorta(primerPregunta);
                actual.setVisible(false);
                respuestaCorta.setVisible(true);
                sistemaGlobal.eliminarPrimerPreguntaCargada();
                actual.dispose();
                break;
            default:
                break;
        }
    }

    /**
     * *
     * Inicializa las variables de Sistema
     *
     * @param archivo recibe la ubicacion absoluta del archivo a cargar
     */
    private void inicializarSistema(String archivo) {
        sistemaGlobal = Sistema.obtenerSistemaGlobal();
        System.out.println("aux inicial");
        aux = new ArchivoLectura(archivo);
        System.out.println("linea error inicia");
        lineaError = 1;
        aux.siguienteLinea();
        System.out.println("in inicia");
        in = aux.linea();
        System.out.println("creo lista pregu vacia");
        sistemaGlobal.crearListaPreguntasVacia();
        System.out.println("creo lista barra vacia");
        sistemaGlobal.crearManejadorBarraProgresoVacio();
        System.out.println("seteo cant preg 0");
        sistemaGlobal.setCantidadPreguntas(0);
        System.out.println("creo registro vacia");
        sistemaGlobal.crearRegistroVacio();
        System.out.println("termine");
    }

    /**
     * *
     * Inicializa las variables que se utilizaran en el constructor de Pregunta
     */
    private void inicializarPregunta() {
        tiempo = 30;
        titulo = "";
        pregunta = "";
        respuestasPosibles = new ArrayList<>();
        comentarios = new ArrayList<>();
        respuestaCorrecta = "";

    }

    /**
     * *
     * Metodo utilizado para avanzar una linea en el archivo lectura; se aumenta
     * en uno la linea de error para llevar control.
     */
    private void avanzarEnArchivo() {
        aux.siguienteLinea();
        lineaError++;
        in = aux.linea();
    }

    /**
     * *
     * Convierte el tiempo del archivo leido en String.
     *
     * @return retorna el tiempo convertido en String
     * @throws ExepcionesGift en caso de estar mal especificado el tiempo lanza
     * una exepcion acorde
     */
    private String cargarTiempo() throws ExepcionesGift {
        String tiempoString = "";
        for (int i = 0; i < in.length(); i++) {
            if (i < 2 && in.charAt(i) != '/') {
                sistemaGlobal.limpiarSistema();
                throw new ExepcionesGift("malEspecificadoTiempo", lineaError);
            }
            if (i > 1) {
                tiempoString += in.charAt(i);
            }
        }
        return tiempoString;
    }

    /**
     * *
     * Comprueba que el tiempo introducido es efectivamente un numero entre 1 y
     * 60
     *
     * @param tiempoString recibe el tiempo en string
     * @throws ExepcionesGift en caso de no ser introducido un numero, o que el
     * tiempo salga del minuto se lanzara un mensaje apropiado
     */
    private void validarTiempo(String tiempoString) throws ExepcionesGift {
        if (!tiempoString.equals("")) {
            try {
                tiempo = Integer.parseInt(tiempoString);
                if (tiempo < 1 || tiempo > 60) {
                    throw new ExepcionesGift("tiempoNoValido", lineaError);
                }
            } catch (NumberFormatException e) {
                throw new ExepcionesGift("tiempoNoValido", lineaError);
            }
        }
    }

    /**
     * *
     * Comprueba que el titulo haya sido introducido correctamente
     *
     * @throws ExepcionesGift lanza una exepcion que avisa al usuario que falta
     * titulo de pregunta en la linea apropiada
     */
    private void validarTitulo() throws ExepcionesGift {
        if (in.charAt(0) != ':' && in.charAt(1) != ':') {
            sistemaGlobal.limpiarSistema();
            throw new ExepcionesGift("faltanTituloPregunta", lineaError);
        }
    }

    /**
     * *
     * Valida si el titulo introducido es correcto
     *
     * @param cont el caracter en la linea en la que estoy
     * @return el caracter despues de realizado el metodo
     * @throws ExepcionesGift
     */
    private int cargarValidarTitulo(int cont) throws ExepcionesGift {
        titulo = "";
        int contadorLocal = cont;
        while (in.charAt(contadorLocal) != ':' && contadorLocal < in.length()) {
            titulo += in.charAt(contadorLocal++);
        }
        if (titulo.equals("")) {
            sistemaGlobal.limpiarSistema();
            throw new ExepcionesGift("faltaTitulo", lineaError);
        }
        return contadorLocal;
    }

    /**
     * *
     * Comprueba si el formato antes del titulo es correcto
     *
     * @param cont el caracter en el que estoy al llamar al metodo
     * @throws ExepcionesGift de no estar esos dos puntos se avisa al usuario
     * con un mensaje de e
     */
    private void validarDosPuntosEntreTituloPregunta(int cont) throws ExepcionesGift {
        if (in.length() <= cont + 2 || in.charAt(cont) != ':'
                || in.charAt(cont + 1) != ':') {
            sistemaGlobal.limpiarSistema();
            throw new ExepcionesGift("errorPrgunta", lineaError);
        }
    }

    /**
     * *
     * Carga la variable pregunta desde el caracter introducido
     *
     * @param cont caracter en el que estoy al llamar al metodo
     */
    private void cargarPreguntaDelArchivo(int cont) {
        int contadorLocal = cont;
        while (contadorLocal < in.length()) {
            pregunta += in.charAt(contadorLocal++);
        }
    }

    /**
     * *
     * Comprueba que la pregunta introducida no sea vacia
     *
     * @throws ExepcionesGift lanza una exepcion en caso que la pregunta
     * introducida sea vacia
     */
    private void validarPregunta() throws ExepcionesGift {
        if (pregunta.equals("")) {
            sistemaGlobal.limpiarSistema();
            throw new ExepcionesGift("faltaPregunta", lineaError);
        }
    }

    /**
     * *
     * valida que el comienzo de llave sea apropiado
     *
     * @throws ExepcionesGift lanza una exepcion apropiada en caso que falte
     * llave de inicio
     */
    private void validarComienzoLlave() throws ExepcionesGift {
        if (in.charAt(0) != '{' || in.length() > 1) {
            sistemaGlobal.limpiarSistema();
            throw new ExepcionesGift("faltaLlave", lineaError);
        }
    }

    /**
     * *
     * Valida que el tipo introducido sea correcto
     *
     * @throws ExepcionesGift lanza una exepcion apropiada en caso que el tipo
     * introducido no sea váildo
     */
    private void validarAsignarTipo() throws ExepcionesGift {
        if (esVerdaderoFalso(aux)) {
            tipo = Pregunta.Tipo.VERDADERO_FALSO;
            respuestaCorrecta += in.charAt(1);
            sistemaGlobal.sumarContadorPregunta();
        } else if (esCorta(aux)) {
            tipo = Pregunta.Tipo.CORTA;
            sistemaGlobal.sumarContadorPregunta();
        } else if (esMultiple(aux)) {
            tipo = Pregunta.Tipo.MULTIPLE;
            sistemaGlobal.sumarContadorPregunta();
        } else {
            sistemaGlobal.limpiarSistema();
            throw new ExepcionesGift("preguntaMalEspecificada", lineaError);
        }
    }

    /**
     * *
     * Construye la pregunta a partir de las variables
     */
    private void cargarPregunta() {
        Pregunta nuevaPregunta = new Pregunta(titulo, pregunta,
                respuestasPosibles, respuestaCorrecta, comentarios,
                tipo, tiempo);
        if (sistemaGlobal.getManejadorBarraProgreso() == null) {
            ArrayList<Pregunta> nuevaLista = new ArrayList<>();
            sistemaGlobal.setManejadorBarraProgreso(nuevaLista);
        }
        if (sistemaGlobal.getListaPreguntas().size() < 10) {
            sistemaGlobal.agregarPreguntaAListaPreguntas(nuevaPregunta);
            sistemaGlobal.agregarPreguntaAListaManejadorBarra(nuevaPregunta);
        }
    }

    /**
     * *
     *
     * @param archivo el archivo que se esta leyendo
     * @return Retorna true si la seccion del archivo tiene las caracteristicas
     * de Verdadero o Falso
     */
    public boolean esVerdaderoFalso(ArchivoLectura archivo) {
        boolean esVerdedaroOFalso = false;
        if (archivo.linea().length() == 3
                && archivo.linea().charAt(0) == '{'
                && archivo.linea().charAt(2) == '}'
                && (archivo.linea().charAt(1) == 'T'
                || archivo.linea().charAt(1) == 'F')) {

            lineaError++;
            if (archivo.siguienteLineaCharAt0() == '}') {
                esVerdedaroOFalso = true;
            } else {
                lineaError--;
            }
        }
        return esVerdedaroOFalso;
    }

    /**
     * *
     *
     * @param archivo el archivo que se esta leyendo
     * @return Retorna true si la seccion del archivo tiene las caracteristicas
     * de opcion multiple
     */
    public boolean esMultiple(ArchivoLectura archivo) {
        int cont = 0;
        int lineasAvanzadas = 0;
        boolean sigoAvanzando = true;
        while (archivo.hayMasLineasSinAvanzar() && sigoAvanzando) {
            in = archivo.linea();
            switch (in.charAt(0)) {
                case '=':
                    if (cont == 0) {
                        cont++;
                        agregarRespuestaPosible();
                        agregarRespuestaCorrecta(in);
                        lineasAvanzadas = avanzarLineaEnEsMultiple(lineasAvanzadas);
                        in = archivo.linea();
                        if (in.charAt(0) == '#') {
                            agregarComentario();
                            lineasAvanzadas = avanzarLineaEnEsMultiple(lineasAvanzadas);
                        } else {
                            sigoAvanzando = false;
                        }
                    } else {
                        sigoAvanzando = false;
                    }
                    break;
                case '~':
                    agregarRespuestaPosible();
                    lineasAvanzadas = avanzarLineaEnEsMultiple(lineasAvanzadas);
                    in = archivo.linea();
                    if (in.charAt(0) == '#') {
                        agregarComentario();
                        lineasAvanzadas = avanzarLineaEnEsMultiple(lineasAvanzadas);
                    } else {
                        sigoAvanzando = false;
                    }
                    break;
                case '}':
                    if (cont == 1) {
                        return true;
                    } else {
                        sigoAvanzando = false;
                        break;
                    }
                default:
                    sigoAvanzando = false;
                    break;
            }
        }
        lineaError -= lineasAvanzadas;
        return false;
    }

    private int avanzarLineaEnEsMultiple(int lineasAvanzadas) {
        aux.siguienteLinea();
        lineaError++;
        return lineasAvanzadas + 1;
    }

    /**
     * *
     * Agrega el resto de la linea al ArrayList de respuestas posibles
     */
    public void agregarRespuestaPosible() {
        String respuestaPosible = "";
        for (int i = 1; i < in.length(); i++) {
            respuestaPosible += in.charAt(i);
        }
        respuestasPosibles.add(respuestaPosible);
    }

    /**
     * *
     * Añade el resto de la linea como una respuesta correcta al String
     *
     * @param linea la linea a leer
     */
    public void agregarRespuestaCorrecta(String linea) {
        String respuestaPosible = "";
        for (int i = 1; i < linea.length(); i++) {
            respuestaPosible += linea.charAt(i);
        }
        respuestaCorrecta = respuestaPosible;
    }

    /**
     * *
     * agrega el resto de la lineas al arraylist de comentarios
     */
    public void agregarComentario() {
        String comentario = "";
        for (int i = 1; i < in.length(); i++) {
            comentario += in.charAt(i);
        }
        comentarios.add(comentario);
    }

    /**
     * *
     * Verifica si la seccion a leer tiene las caracteristicas de pregunta corta
     *
     * @param archivo el archivo que se esta leyendo
     * @return true si tiene caracteristicas de pregunta corta
     */
    public boolean esCorta(ArchivoLectura archivo) {
        int cont = 0;
        int lineasAvanzadas = 0;
        String respuestasCorrectas = "=";
        String linea = archivo.linea();
        while (archivo.hayMasLineasSinAvanzar()) {
            switch (linea.charAt(0)) {
                case '=':
                    cont++;
                    String respuestaNueva = "";
                    for (int i = 1; i < linea.length(); i++) {
                        respuestaNueva += linea.charAt(i);
                    }
                    respuestasCorrectas += respuestaNueva + "@splitargument@";
                    archivo.siguienteLinea();
                    lineasAvanzadas++;
                    lineaError++;
                    linea = archivo.linea();
                    break;
                case '}':
                    if (cont < 2) {
                        lineaError -= lineasAvanzadas;
                    } else {
                        agregarRespuestaCorrecta(respuestasCorrectas);
                        return true;
                    }
                    break;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * *
     * Confirma si el usuario quiere cerrar la ventana actual y volver al menu
     * principal
     *
     * @param actual la ventana actual
     */
    public void volverAlMenu(JFrame actual) {
        int resp = JOptionPane.showConfirmDialog(null,
                "¿Está seguro que desea cancelar la partida y volver al menú? ¡Perderá su progreso!");
        if (resp == 0) {
            MenuPrincipal menu = new MenuPrincipal();
            menu.setVisible(true);
            actual.setVisible(false);
            actual.dispose();
        }
    }

}
