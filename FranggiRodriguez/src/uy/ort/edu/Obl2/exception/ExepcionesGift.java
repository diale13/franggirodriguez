/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uy.ort.edu.Obl2.exception;

/**
 * @author Franggi y Rodriguez
 */
public class ExepcionesGift extends Exception {

    private final String codigoError;
    private final int linea;

    /**
     * *
     * Se crea una ExepcionGift acorde a un código de error y la linea donde
     * ocurre un error
     *
     * @param codigoError el codigo de error: malEspecificadoTiempo,
     * tiempoNoValido, faltanTituloPregunta, errorPrgunta, faltaTitulo,
     * preguntaMalEspecificada,faltaLlave
     * @param lineaDondeOcurre refiere a la linea del gift donde ocurre el error
     */
    public ExepcionesGift(String codigoError, int lineaDondeOcurre) {
        super();
        this.codigoError = codigoError;
        this.linea = lineaDondeOcurre;
    }

    /**
     * *
     *
     * @return el string con el mensaje de error apropado
     */
    @Override
    public String getMessage() {
        String mensaje = "";
        switch (codigoError) {
            case "malEspecificadoTiempo":
                mensaje = "Hay un error en el formato del gift en la linea "
                        + linea + " ,comprobar que el tiempo introducido sea un número";
                break;
            case "tiempoNoValido":
                mensaje = "El tiempo introducido en la linea "
                        + linea + " no es un número entre 1-60, comprobar el archivo introducido";
                break;
            case "faltanTituloPregunta":
                mensaje = "Error en la linea"
                        + linea + "no se ha introducido ni titulo ni pregunta, comprobar el archivo introducido";
                break;
            case "errorPrgunta":
                mensaje = "Error en la linea " + linea
                        + ". No es correcto el formato, verifique que respete el formato de ejemplo";
                break;
            case "faltaTitulo":
                mensaje = "Error en linea " + linea + " falta titulo para la pregunta ";
                break;
            case "preguntaMalEspecificada":
                mensaje = "Error en la línea "
                        + linea + ". Verifique que respete el formato de ejemplo.";
                break;
            case "faltaLlave":
                mensaje = "Ha ocurrido un error de cierre inesperado en la linea"
                        + linea + "verifique que las llaves curvas esten colocadas correctamente";
                break;
            default:
                mensaje = "Ha ocurrido un error inesperado dentro del archivo gift";
        }

        return mensaje;

    }

}
